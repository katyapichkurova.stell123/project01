<?require 'blocks/header.php';?>

<section class="service">
        <div class="container">
                <div class="row justify-content-center">
                        <div class="col-xl-8">
                                <h2 class="about__title">Тарифы</h2>
                                <p class="about__text">Здесь вы можете выбрать подходящий тариф!</p>
                </div>
                </div>
                <div class="row justify-content-around">
                       <div class="col-xl-4 col-md-6">
                        <div class="price">
                                
                                <div class="price__head">
                                       <p>Стартовый<br/></p> 
                                        <sup>₽</sup> <span id="free" >3 000</span><sub> в месяц</sub>
                                </div>
                                <div class="wrapper">
                                        <p>Срок подключения</p>

                                        <input type="radio" name="slider" id="tab-1">
                                        <input type="radio" name="slider" id="tab-2" >
                                        <input type="radio" name="slider" id="tab-3">
                                    
                                        <header>
                                            <label for="tab-1" class="tab-1">1 мес.</label>
                                            <label for="tab-2" class="tab-2">3 мес.</label>
                                            <label for="tab-3" class="tab-3">1 год.</label>
                                            <div class="slider"></div>
                                    
                                        </header>
                                        
                                </div>
                                <div class="price__body">
                                        <ul>
                                                <li>10 исходящих сообщений в день</li>
                                                <li>100 лимит товаров</li>
                                                <li>Показ вашего телефона незарегистрированным посетителям</li>
                                                <li>Показ вашего E-mail и ссылки на ваш сайт/ соцсети</li>
                                                <li>Просмотр сайта без рекламы</li>
                                                <li>Статус верифицированной компании (при оплате по счету)</li>

                                        </ul>
                                </div>
                                <div class="price__footer">
                                        <button class="button"><a  class="price_a" style="color:black;" href="/tarif-check.php?type=free">Подключить тариф</a></button>
                                </div>
                        </div>
                       </div>
                       <div class="col-xl-4 col-md-6">
                        <div class="price">
                                <div class="price__head">
                                       <p>Премиум<br/></p> 
                                        <sup>₽</sup><span id="premium"> 5 500</span><sub> в месяц</sub>
                                </div>
                                <div class="wrapper">
                                        <p>Срок подключения</p>

                                        <input type="radio" name="slider" id="tab11-1">
                                        <input type="radio" name="slider" id="tab11-2" checked>
                                        <input type="radio" name="slider" id="tab11-3">
                                    
                                        <header>
                                            <label for="tab11-1" class="tab11-1">1 мес.</label>
                                            <label for="tab11-2" class="tab11-2">3 мес.</label>
                                            <label for="tab11-3" class="tab11-3">1 год.</label>
                                            <div class="slider"></div>
                                    
                                        </header>
                                        
                                </div>
                                <div class="price__body">
                                        <ul>
                                                <li>1 200 открытий закупок (на срок 12 мес.)</li>
                                                <li>100 исходящих сообщений в день</li>
                                                <li>10 000 лимит товаров</li>
                                                <li>Показ вашего телефона незарегистрированным посетителям </li>
                                                <li>Просмотр контактов поставщиков</li>
                                                <li>Отключение сторонней рекламы на вашей странице и просмотр сайта без рекламы</li>
                                                <li>Ежедневная синхронизация товаров YML
                                                </li>
                                                <li>Статус верифицированной компании (при оплате по счету)</li>

                                        </ul>
                                </div>
                                <div class="price__footer">
                                        <button class="button" ><a class="price_a" style="color:black;" href="/tarif-check.php?type=premium">Подключить тариф</a></button>
                                </div>
                        </div>
                       </div>
                       <div class="col-xl-4 col-md-6">
                        <div class="price">
                                <div class="price__head">
                                       <p>Бизнес<br/></p> 
                                        <sup>₽</sup><span id="bussnes">9 000</span><sub> в месяц</sub>
                                </div>
                                <div class="wrapper">
                                        <p>Срок подключения</p>

                                        <input type="radio" name="slider" id="tab1-1">
                                        <input type="radio" name="slider" id="tab1-2">
                                        <input type="radio" name="slider" id="tab1-3">
                                    
                                        <header>
                                            <label for="tab1-1" class="tab1-1">1 мес.</label>
                                            <label for="tab1-2" class="tab1-2">3 мес.</label>
                                            <label for="tab1-3" class="tab1-3">1 год.</label>
                                            <div class="slider"></div>
                                    
                                        </header>
                                        
                                </div>
                                <div class="price__body">
                                        <ul>
                                                <li>1 200 открытий закупок (на срок 12 мес.)</li>
                                                <li>30 исходящих сообщений в день</li>
                                                <li>1 000 лимит товаров</li>
                                                <li>Показ вашего телефона незарегистрированным посетителям </li>
                                                <li>Просмотр контактов поставщиков</li>
                                                <li>Отключение сторонней рекламы на вашей странице и просмотр сайта без рекламы</li>
                                                <li>Ежедневная синхронизация товаров YML
                                                </li>
                                                <li>Статус верифицированной компании (при оплате по счету)</li>

                                        </ul>
                                </div>
                                <div class="price__footer">
                                        <button id="bussness" class="button"><a class="price_a" style="color:black;" href="/tarif-check.php?type=bussness">Подключить тариф</a></button>
                                </div>
                        </div>
                       </div>
                </div>
        </div>
</section>

<?require 'blocks/footer.php';?>