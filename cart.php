<?require 'blocks/header.php';?>
<section class="service">
        <div class="container">
                <div class="row justify-content-center">
                        <div class="col-xl-12 col-12">
                               <div class="breadcrumbs">
                                        <a href="">Главная</a> / <a href="">Каталог</a> / <a href="">Светильник настольный "Белый лебедь"</a>                          
                               </div>
                               
                </div>

                </div>
                
        </div>


        <div class="container">
        <?
require 'tel.php';
$cart_id=$_GET['cart_id'];
require 'configDB.php';
$query=$pdo->query("SELECT * FROM `description` ORDER BY `id` DESC");

while ($row =$query->fetch(PDO::FETCH_OBJ)) {
  $tel=(string)$row->tel;
$tel=phone_format($tel);
//echo  $tel;
if($cart_id==$row->id){
?>  
        <div class="gallery-main">
    
    
          <div class="gallery" style="background-color:white;">
            <?php 
            $anons="uploads/zav-$row->id.jpg";
            if(file_exists($anons)){

            ?>
            <img src="uploads/zav-<?=$row->id?>.jpg" class="gallery-height" />
            <? }else{

            ?>
     <img src="img/no_photo.png" class="gallery-height" />
     <?php } ?>
      <div class="preview">
        <?php
$i = 0;
$image="";
while($i <= 3){
  $image="uploads/p-$row->id$i.jpg";
 // echo  $image;
  if (file_exists($image)){
        ?>
        <img src="uploads/p-<?=$row->id?><?=$i?>.jpg" width="10px" />

    <?php
    $i++;
  }else{$i++;}

  
}

?>
      </div>
    
    
          </div>
          <div class="gallery-description">
        <h1><?=$row->description?></h1>
        <span class="card-meta"><div class="tag" style="font-size:25px;margin-right:20px;"><i class="fa fa-tag"></i><?=$row->price?></div><div id="tell" style="
                float: left;">
                              <a href="<?=$row->tel?>" style="float:right;" class="phone_number"><?=$tel?></a><span class="phone_number_active show">Показать </span>
                    </div></span>
            <p><strong>Местоположение: </strong>Пермский край, Пермь, <?=$row->district?> район.</p>
            <p><strong>Категория: </strong>Животные.</p>
            <p><strong>Размещено: </strong><?=$row->timeads?></p>
            <p><strong>Описание: </strong> <?=$row->des?></p>
    
      </div>
  
      <?
}}
?>
</div>
<div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="text-center mt-5">
                    Комментарии
                </h2>
            </div>
            <div class="col-lg-6">
                <div id="comment-field" >
                <?
$cart_id=$_GET['cart_id'];

require 'configDB.php';
$query=$pdo->query("SELECT * FROM `reviews` ORDER BY `id_review` DESC");

while ($row =$query->fetch(PDO::FETCH_OBJ)) {
if($cart_id==$row->id_review){
?>  
 <p class="text-right small"><em><?=$row->time?></em></p>
       <p class="alert alert-success" style="text-align:left;" role="alert">Имя: <?=$row->name?>
        <br/>Сообщение:<?=$row->review?></p>
<?
}}
?>
                </div>
            </div>
            <div class="col-lg-6">
                    <form method="" action="">
                            
                              <input type="text" name="comment-name" id="comment-name"  placeholder="Твое имя">
                          
                        
                              <textarea type="text" name="comment-body" id="comment-body" placeholder="Твой комментарий"></textarea>
                       
                              <input type="submit" id="comment-add" value="Добавить комментарий" />


                          
                          </form>
            </div>
        </div>
    </div>
      </section>

      <script type="text/javascript">
        let searchParams = new URLSearchParams(window.location.search);
       
        let param = searchParams.get('cart_id')
     
var d = new Date();
var month = d.getMonth()+1;
var day = d.getDate();
var dt = new Date();
var output = d.getFullYear() + '.' +
    (month<10 ? '0' : '') + month + '.' +
    (day<10 ? '0' : '') + day;
var time = output+" "+dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();



  $("#comment-add").click(function(){
    event.preventDefault();
    var commentName = $('#comment-name').val();
    var commentBody = $( "#comment-body" ).val();
   // let commentName = document.getElementById('comment-name');
   // let commentBody = document.getElementById('comment-body');
   // alert( commentName);
    if(commentName=='' || commentBody==''){
      alert("Заполните поля");
    }else{
      
    
    
    
     

  $.post("data.php", 
    {
        name:commentName,
        city:commentBody,
        time:time,
        param:param
    },
    
    function(data, status){
     alert("Готово");
     location.reload();
    });
  }
  });
</script>

      <script type="text/javascript">
          /*  

        let comments = [];
//loadComments();

document.getElementById('comment-add').onclick = function(){
  event.preventDefault();
    let commentName = document.getElementById('comment-name');
    let commentBody = document.getElementById('comment-body');
    
    if(commentName.value=='' || commentBody.value==''){
      alert("Заполните поля");
    }else {
      let comment = {
        name : commentName.value,
        body : commentBody.value,
        time : Math.floor(Date.now() / 1000)

    }


    commentName.value = '';
    commentBody.value = '';

   comments.push(comment);
   // saveComments();
  //  showComments();
    }
    
}




function saveComments(){
    localStorage.setItem('comments', JSON.stringify(comments));
}

function loadComments(){
    if (localStorage.getItem('comments')) comments = JSON.parse(localStorage.getItem('comments'));
    showComments();
}

function showComments (){
    let commentField = document.getElementById('comment-field');
    let out = '';
    comments.forEach(function(item){
        out += `<p class="text-right small"><em>${timeConverter(item.time)}</em></p>`;
        out += `<p class="alert alert-success" style="text-align:left;" role="alert">Имя: ${item.name}`;
        out += `<br/>Сообщение: ${item.body}</p>`;
    });
    commentField.innerHTML = out;
}

function timeConverter(UNIX_timestamp){
    var a = new Date(UNIX_timestamp * 1000);
    var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var date = a.getDate();
    var hour = a.getHours();
    var min = a.getMinutes();
    var sec = a.getSeconds();
    var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
    return time;
  }

  
*/

  </script>



      <script type="text/javascript">
        function imageGallery(){
    
          const height=document.querySelector(".gallery-height");
          const previews=document.querySelectorAll(".preview img");
          previews.forEach(preview => {
            preview.addEventListener("click",function(){
            const smallSrc=this.src;
            const bigSrc=smallSrc.replace("small","big");
            height.src=bigSrc;
              console.log(bigSrc);
            });
          });
    
        }
        imageGallery();
    
        $(function(){
        let holder=$('.phone_number'),
        button=$('.phone_number_active'),
        number=holder.text(),
        symbolsForHide=9,
        show=()=>{
          holder.text(number)
          button.removeClass('show').text('Скрыть')
        },
        hide=()=>{
          holder.text(number.replace(new RegExp('(.+).{'+symbolsForHide+'}$'),"$1"+'x'.repeat(symbolsForHide)))
          button.addClass('show').text('Показать ')
        }
        button.click(function(){
          if($(this).hasClass('show')) show()
          else hide()
        })
        hide()
        })
    

      </script>
 
     <!-- Contact -->
  <section id="contact" class="four">
        <div class="container">
    
                <div class="row justify-content-center">
                        <div class="col-xl-12">
                                <h2 class="about__title">Отправить предложение на заказ</h2>
                                <p class="about__text">Заполните форму: укажите цену за заказ и срок поставки или задайте вопрос. После этого ваше предложение получит заказчик. Если заказчик выберет вас, мы отправим вам его контактные данные. Нажимая кнопку «Отправить предложение», вы соглашаетесь с условиями использования и обработкой персональных данных. Ваше предложение увидит только заказчик.</p>
                </div>
    
          
                <div class="col-xl-12">
          <form method="post" action="#">
            <div class="row">
                <div class="col-xl-6">
                        <textarea name="message" placeholder="Message"></textarea>
                </div>
              <div class="col-xl-3 col-md-6">
                <input type="text" name="name" placeholder="Цена" />
                <input type="text" name="name" placeholder="Фамилия и имя" />
                <input type="text" name="name" placeholder="E-mail" />

        </div>
              <div class="col-xl-3 col-md-6">
                <input type="text" name="email" placeholder="Поставка, дни" />
                <input type="text" name="name" placeholder="Название компании" />
                <input type="text" name="name" placeholder="Телефон" />
                <input type="submit" value="Отправить предложение" />

        </div>
   
            </div>
          </form>
          </div>
    
        </div>
      </section> 
      <section class="about"  id="company">
        <div class="container">
                <div class="row justify-content-center">
                        <div class="col-xl-8">
                                <h2 class="about__title">Похожие заказы</h2>
                                <p class="about__text">Рассмотрите заказы у других поставщиков</p>
                </div>
                </div>
                <?
                  require 'configDB.php';
                  $query=$pdo->query("SELECT * FROM `description` ORDER BY `id` ASC");
                  $k=1;
                 
                  
                  while ($row =$query->fetch(PDO::FETCH_OBJ)) {
                    $image="uploads/zav-$row->id.jpg";
                  echo '
                  <div class="col-xl-12" id="order'.$k.'" style="display:none;">
                  <a  href="/cart.php?cart_id='.$row->id.'" >
                     <div class="order">';
if (file_exists($image)){
                     echo'
                             <img src="uploads/zav-'.$row->id.'.jpg" width="250px" style="max-width:100%;"/>
                             <div class="order-text">
                                     <h5>'.$row->description.'</h5>
                                     <span class="data" >'.$row->timeads.'</span>
                                     <br/>
                                     <span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star"></span>
<span class="fa fa-star"></span>
                                     <p>
             '.$row->des.'
                                     </p>
             <span class="card-meta"><div class="tag"><i class="fa fa-tag"></i>'.$row->price.'₽</div></span><br><br>
                                     <button class="button">Подробнее</button>

                             </div>
                             
                             
                     </div>
     </a></div>';}else{
      echo'
      <img src="img/no_photo.png" width="250px" style="max-width:100%;"/>
      <div class="order-text">
              <h5>'.$row->description.'</h5>
              <span class="data" >'.$row->timeads.'</span>
              <br/>
              <span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star"></span>
<span class="fa fa-star"></span>
              <p>
'.$row->des.'
              </p>
<span class="card-meta"><div class="tag"><i class="fa fa-tag"></i>'.$row->price.'₽</div></span><br><br>
              <button class="button">Подробнее</button>

      </div>
      
      
</div>
</a></div>';
     }

     $k++;  }
                     ?>
                <div class="row">
                        <div class="col-xl-12">
                           <button class="button_blog mb-5" id="5">Смотреть ещё</button>
                        </div>
                     </div>
                    
        </div>
</section>
<script type="text/javascript">

let k=0;
  while (k < 5) { // выводит 0, затем 1, затем 2
    $('#order'+k).show(500);
  k++;
  }
</script>
        <script type="text/javascript">
      


$( ".button_blog " ).click(function() {
  let i =0;
  var clickId = $(this).prop('id');
  while (i < clickId) { // выводит 0, затем 1, затем 2
    $('#order'+i).show(500);
  i++;
}
let age = Number(clickId);
age=age+5;

$(this).attr("id", age);
});


</script>
<?require 'blocks/footer.php';?>
<? //<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>?>
