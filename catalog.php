<?require 'blocks/header.php';?>
<section class="service">
        <div class="container">
                <div class="row justify-content-center">
                        <div class="col-xl-12 col-12">
                               <div class="breadcrumbs">
                                        <a href="">Главная</a> / <a href="">Каталог</a> / <a href="">Светильник настольный "Белый лебедь"</a>                          
                               </div>
                               
                </div>
                </div>
                <div class="row ">
                        <div class="col-xl-12">
                                <h2 class="about__title left">Заказы</h2>
                                <p class="about__text left">Разместите заказ бесплатно за 2 минуты и получите выгодные предложения!</p>
                </div>
                </div>
             <div class="row">
                <div class="col-xl-3">
                                <div class="sidenav">
                                        <h4>Категории</h4>
                                        <a href="/catalog.php?category=dog">Для собак</a>
                                        <a href="/catalog.php?category=cat">Для кошек</a>
                                        <a href="/catalog.php?category=birds">Для птиц</a>
                                        <a href="/catalog.php?category=reptiles">Для рептилии</a>
                                        <a href="/catalog.php?category=fish">Для рыб</a>
                                        <a href="/catalog.php?category=cleanliness">Гигиена и чистота</a>
                                        <a href="/catalog.php?category=feed">Корм</a>
                                        <a href="/catalog.php?category=stroll">Для прогулок</a>
                                      <!-- <button class="dropdown-btn">Dropdown
                                          <i class="fa fa-caret-down"></i>
                                        </button>
                                        <div class="dropdown-container">
                                          <a href="#">Link 1</a>
                                          <a href="#">Link 2</a>
                                          <a href="#">Link 3</a>
                                        </div>-->
                                        
                                </div>
                                <div class="sidenav_2">
                                        <h4>Объём заказа (руб.)</h4>
                                        <div class="r-all">
                                        
                                                <span class="r-group">
                                                    <input class="r-input" type="radio" name="radioname" id="radio-1" checked onclick="document.location='catalog.php?size=10'"/>
                                                    <label for="radio-1"></label>
                                                    <span>10 тыс.</span>
                                                </span>
                                          
                                                <span class="r-group">
                                                    <input class="r-input" type="radio" name="radioname" id="radio-2" onclick="document.location='catalog.php?size=100'"/>
                                                    <label for="radio-2"></label>
                                                    <span>100 тыс.</span>
                                                </span>
                                                <span class="r-group">
                                                    <input class="r-input" type="radio" name="radioname" id="radio-3" onclick="document.location='catalog.php?size=300'"/>
                                                    <label for="radio-3"></label>
                                                    <span>300 тыс.</span>
                                                </span>
                                                <span class="r-group">
                                                    <input class="r-input" type="radio" name="radioname" id="radio-4" onclick="document.location='catalog.php?size=1000000'"/>
                                                    <label for="radio-4"></label>
                                                    <span>1 млн</span>

                                                </span> 
                                              <span class="r-group">
                                                    <input class="r-input" type="radio" name="radioname" id="radio-5" onclick="document.location='catalog.php?size=500000'"/>
                                                    <label for="radio-5"></label>
                                                    <span>5 млн</span>

                                                </span> 
                                            </div>
                                </div>
                                <? $id=$_COOKIE['id'];
      
      if($_COOKIE["id"]!=""){
                echo '<button class="button mb-3 add" style="width:100%; margin:0px;"  ><a href="/request.php?id='.$id.'">Добавить заказ</a></button>';
      }else{}
?>
                                
                       
                </div>
                <div class="col-xl-9">
                <?
                $category_name="0";
                $size="0";
                  require 'configDB.php';
                  switch ($_GET['category']) {
                    case "dog":
                      $category_name="Для собак";
                        break;
                    case "cat":
                      $category_name="Для кошек";
                        break;
                    case "birds":
                      $category_name="Для птиц";
                        break;
                    case "reptiles":
                      $category_name="Для рептилии";
                          break;   
                    case "fish":
                      $category_name="Для рыб";
                          break;
                    case "cleanliness":
                      $category_name="Гигиена и чистота";
                          break;
                    case "feed":
                      $category_name="Корм";
                          break;
                    case "stroll":
                      $category_name="Для прогулок";
                          break;
                }

                switch ($_GET['size']) {
                  case "10":
                    $size="10 тыс.";
                      break;
                  case "100":
                    $size="100 тыс.";
                      break;
                  case "300":
                    $size="300 тыс.";
                      break;
                  case "1000000":
                    $size="1 млн.";
                        break;   
                  case "500000":
                    $size="5 млн.";
                        break;
              }
             
               
                  $k=1;
                  $query=$pdo->query("SELECT * FROM `description` ORDER BY `category` ASC");
                  $image="";
                  while ($row =$query->fetch(PDO::FETCH_OBJ)) {
                    if($row->category==$category_name || $row->size==$size){
                      $image="uploads/zav-".$row->id.".jpg";
                      if (file_exists($image)){
                  echo '
                  <div class="col-xl-12" id="order'.$k.'" style="display:none;">
                  <a  href="/cart.php?cart_id='.$row->id.'" >
                     <div class="order">
                             <img src="uploads/zav-'.$row->id.'.jpg"" width="250px" style="max-width:100%;"/>
                             <div class="order-text">
                                     <h5>'.$row->description.'</h5>
                                     <span class="data" >'.$row->timeads.'</span>
                                     <br/>
                                     <span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star"></span>
<span class="fa fa-star"></span>
                                     <p>
             '.$row->des.'
                                     </p>
             <span class="card-meta"><div class="tag"><i class="fa fa-tag"></i>'.$row->price.'₽</div></span><br><br>
                                     <button class="button">Подробнее</button>

                             </div>
                             
                             
                     </div>
     </a></div>';
     $k++;}else{
      echo '
                  <div class="col-xl-12" id="order'.$k.'" style="display:none;">
                  <a  href="/cart.php?cart_id='.$row->id.'" >
                     <div class="order">
                             <img src="img/no_photo.png" width="250px" style="max-width:100%;"/>
                             <div class="order-text">
                                     <h5>'.$row->description.'</h5>
                                     <span class="data" >'.$row->timeads.'</span>
                                     <br/>
                                     <span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star"></span>
<span class="fa fa-star"></span>
                                     <p>
             '.$row->des.'
                                     </p>
             <span class="card-meta"><div class="tag"><i class="fa fa-tag"></i>'.$row->price.'₽</div></span><br><br>
                                     <button class="button">Подробнее</button>

                             </div>
                             
                             
                     </div>
     </a></div>';
     $k++;
     }
                      }elseif($category_name=="0" && $size=="0" ){
                        $image="uploads/zav-".$row->id.".jpg";
                        if (file_exists($image)){
                        echo '
                        <div class="col-xl-12" id="order'.$k.'" style="display:none;">
                        <a  href="/cart.php?cart_id='.$row->id.'" >
                           <div class="order">
                                   <img src="uploads/zav-'.$row->id.'.jpg"" width="250px" style="max-width:100%;"/>
                                   <div class="order-text">
                                           <h5>'.$row->description.'</h5>
                                           <span class="data" >'.$row->timeads.'</span>
                                           <br/>
                                           <span class="fa fa-star checked"></span>
      <span class="fa fa-star checked"></span>
      <span class="fa fa-star checked"></span>
      <span class="fa fa-star"></span>
      <span class="fa fa-star"></span>
                                           <p>
                   '.$row->des.'
                                           </p>
                   <span class="card-meta"><div class="tag"><i class="fa fa-tag"></i>'.$row->price.'₽</div></span><br><br>
                                           <button class="button">Подробнее</button>
      
                                   </div>
                                   
                                   
                           </div>
           </a></div>';
           $k++;
                         }else{
                          echo '
                        <div class="col-xl-12" id="order'.$k.'" style="display:none;">
                        <a  href="/cart.php?cart_id='.$row->id.'" >
                           <div class="order">
                                   <img src="img/no_photo.png" width="250px" style="max-width:100%;"/>
                                   <div class="order-text">
                                           <h5>'.$row->description.'</h5>
                                           <span class="data" >'.$row->timeads.'</span>
                                           <br/>
                                           <span class="fa fa-star checked"></span>
      <span class="fa fa-star checked"></span>
      <span class="fa fa-star checked"></span>
      <span class="fa fa-star"></span>
      <span class="fa fa-star"></span>
                                           <p>
                   '.$row->des.'
                                           </p>
                   <span class="card-meta"><div class="tag"><i class="fa fa-tag"></i>'.$row->price.'₽</div></span><br><br>
                                           <button class="button">Подробнее</button>
      
                                   </div>
                                   
                                   
                           </div>
           </a></div>';
           $k++;
                         } }
                   
                    
                    }
                     ?>
                        
                        
                </div>
               

             </div>
             <div class="row justify-content-center">
                <div class="col-xl-12">
                   <button class="button_blog mb-5" id="5">Смотреть ещё</button>
                </div>
             </div>
                </div>
                
        </div>
        <script type="text/javascript">

let k=0;
  while (k < 5) { // выводит 0, затем 1, затем 2
    $('#order'+k).show(500);
  k++;
  }
</script>
        <script type="text/javascript">
        var dropdown = document.getElementsByClassName("dropdown-btn");
var i;

for (i = 0; i < dropdown.length; i++) {
  dropdown[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var dropdownContent = this.nextElementSibling;
    if (dropdownContent.style.display === "block") {
      dropdownContent.style.display = "none";
    } else {
      dropdownContent.style.display = "block";
    }
  });
}

let all = document.querySelectorAll('.r-all');
for (let a = 0; a < all.length; a++){ 
    let radios = all[a].querySelectorAll('.r-input');
    let i = 1;
    all[a].style.setProperty('--options',radios.length);
    radios.forEach((input)=>{
        input.setAttribute('data-pos',i);
        input.addEventListener('click',(e)=>{
            all[a].style.setProperty('--options-active',e.target.getAttribute('data-pos'));
        });
        i++;
    });
};    



$( ".button_blog " ).click(function() {
  let i =0;
  var clickId = $(this).prop('id');
  while (i < clickId) { // выводит 0, затем 1, затем 2
    $('#order'+i).show(500);
  i++;
}
let age = Number(clickId);
age=age+5;

$(this).attr("id", age);
});


</script>


<?require 'blocks/footer.php';?>