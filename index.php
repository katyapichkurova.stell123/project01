<?require 'blocks/header.php';?>
<header class="header">
        <div class="container">
                <div class="row">
                        <div class="col-xl-12">
                                <h1 class="header__title">Поможем найти поставщиков и продать товары для животных </h1>
                                <p class="header__text"> В короткие сроки 
                                        с помощью нашей платформы</p>
                                <button class="button" onclick="document.location='register.php'">Продать товары</button>
                                <button class="button" onclick="document.location='catalog.php'">Купить товары</button>
                        </div>
                </div>
        </div>
</header>
<section class="about"  id="company">
        <div class="container">
                <div class="row justify-content-center">
                        <div class="col-xl-8">
                                <h1 class="about__title">О компании</h1>
                                <p class="about__text">На платформе вы получите следующие средства!</p>
                </div>
                </div>
                <div class="row justify-content-around mb-5">
                        <div class="col-xl-3 col-md-6">
                                <img src="img/internet.png" alt="">
                                <h3 class="about_h3">Дополнительная точка для продаж в интернете</h3>
                        </div>
                        <div class="col-xl-3 col-md-6">
                                <img src="img/transfer.png" alt="">
                                <h3 class="about_h3">Звонки и заявки от новых клиентов</h3>
                        </div>
                        <div class="col-xl-3 col-md-6">
                                <img src="img/head.png" alt="">
                                <h3 class="about_h3">Регулярный поток целевых покупателей</h3>
                        </div>
                        <div class="col-xl-3 col-md-6">
                                <img src="img/sale.png" alt="">
                                <h3 class="about_h3">Быстрый старт онлайн-продаж</h3>
                        </div>

                </div>
                     <div class="row right justify-content-between">
                        <div class="blog-item">
                           <img src="img/blog4.jpg" alt="" class="img-fluid">
                        </div>
                        <div class="blog-item">
                           <div class="blog">
                              <a href="">Первый Поставщик </a>
                              <div>
                                 <span><i class="far fa-calendar-alt"></i> 14.07.2017</span>
                                 <span><i class="far fa-file"></i> Development</span>
                              </div>
                              <p>Это бесплатная база поставщиков и производителей 
                                по поиску поставщиков. Помогает налаживать контакты с потенциальными партнерами и заказчиками 
                                по Перми и находить новых клиентов. </p>
                                <button class="button" onclick="document.location='company.php'">Подробнее</button>
                           </div>
                        </div>
                     </div>
        </div>
</section>
<section class="catalog">
        <div class="container">
                <div class="row justify-content-center">
                        <div class="col-xl-8">
                                <h2 class="about__title">Каталог</h2>
                                <p class="about__text">Компания "Первый поставщик" предоставляет различные товары для домашних животных</p>
                </div>
                </div>
                <div class="row">
                        <div   class="col-xl-3 col-md-6">
                        <a href="/catalog.php?category=dog">
                                <div class="cart">
                                        
                                        <img src="img/cart1.png" alt="">
                                        <h3>Для собак</h3>
                                        
                                </div></a>
                        </div>
                        <div   class="col-xl-3 col-md-6">
                        <a href="/catalog.php?category=cat">
                                <div class="cart">
                                        <img src="img/cart2.png" alt="">
                                        <h3>Для кошек</h3>

                                </div></a>
                        </div>
                        <div   class="col-xl-3 col-md-6">
                        <a href="/catalog.php?category=birds">
                                <div class="cart">
                                        <img src="img/cart3.png" alt="">
                                        <h3>Для птиц</h3>

                                </div></a>
                        </div>
                        <div   class="col-xl-3 col-md-6">
                        <a href="/catalog.php?category=reptiles">
                                <div class="cart">
                                        <img src="img/cart4.png" alt="">
                                        <h3>Для рептилии</h3>

                                </div>
                                </a>
                        </div>
                        <div   class="col-xl-3 col-md-6">
                        <a href="/catalog.php?category=fish">
                                <div class="cart">
                                        <img src="img/cart5.png" alt="">
                                        <h3>Для рыб</h3>

                                </div>
                                </a>
                        </div>
                        <div   class="col-xl-3 col-md-6">
                        <a href="/catalog.php?category=cleanliness">
                                <div class="cart">
                                        <img src="img/cart8.png" alt="">
                                        <h3>Гигиена и чистота</h3>

                                </div>
                                </a>
                        </div>
                        <div   class="col-xl-3 col-md-6">
                        <a href="/catalog.php?category=feed">
                                <div class="cart">
                                        <img src="img/cart6.png" alt="">
                                        <h3>Корм</h3>

                                </div>
                                </a>
                        </div>
                        <div   class="col-xl-3 col-md-6">
                        <a href="/catalog.php?category=stroll">
                                <div class="cart">
                                        <img src="img/cart7.png"  alt="">
                                        <h3>Для прогулок</h3>

                                </div>
                                </a>
                        </div>
                       

                </div>
               
        </div>
</section>
<section class="service">
        <div class="container">
                <div class="row justify-content-center">
                        <div class="col-xl-8">
                                <h2 class="about__title">А хотите, мы найдем то, что вам нужно?</h2>
                                <p class="about__text">Расскажите, что вы ищете!
                                        Мы отправим вашу заявку компаниям с похожими предложениями.
                                        С вами свяжется представитель компании продавца и расскажет о наличии и ценах.</p>
                </div>

                </div>
                <div class="row">
                        <div class="col-xl-12">
                                <button class="button_service" onclick="document.location='search.php'">Найти товар</button>
                        </div>
                </div>
        </div>
</section>
<section class="portfolio">
        <div class="container">
                <div class="row justify-content-center">
                        <div class="col-xl-8">
                                <h2 class="about__title">Новости</h2>
                                <p class="about__text">Последние самые важные и актуальные новости сайта товаров для животных!</p>
                                <button val="on" class="button" filter="all">Все</button>
                                <button val="off" class="button" filter="wd">Грумминг</button>
                                <button val="off" class="button" filter="ud">Лечение</button>
                                <button val="off" class="button" filter="moc">Еда</button>

                </div>
                </div>
                <div class="row filter">
                <?
                  require 'configDB.php';
                  $query=$pdo->query("SELECT * FROM `news` ORDER BY `id` ASC");

                  while ($row =$query->fetch(PDO::FETCH_OBJ)) {
                  echo '
                        <div  filter="'.$row->filter.'" class="col-xl-4 col-md-6">
                        <a href="/new.php?id='.$row->id.'">
                                <div class="text">
                                        <img src="'.$row->img.'" alt="">
                                        <h5>'.$row->header.'</h5>
                                        <p>'.$row->anons.'</p>
                                        <span class="data" >'.$row->time.'</span>

                                </div>
                                </a>
                        </div>';}?>
                        

                </div>
                <div class="row">
                        <div class="col-xl-12">
                                <button class="button_service" onclick="document.location='news.php'">Смотреть ещё</button>
                        </div>
                </div>
        </div>
</section>
<section class="about"  id="company">
        <div class="container">
                <div class="row justify-content-center">
                        <div class="col-xl-8">
                                <h1 class="about__title">Попробуйте Первый Поставщик бесплатно для продаж в интернете</h1>
                                <p class="about__text">Вы получите следующие преимущества!</p>
                </div>
                </div>
                <div class="row justify-content-around">
                        <div class="col-xl-3 col-md-6">
                                <img src="img/internet.png" alt="">
                                <h3 class="about_h3">Дополнительная точка для продаж в интернете</h3>
                        </div>
                        <div class="col-xl-3 col-md-6">
                                <img src="img/transfer.png" alt="">
                                <h3 class="about_h3">Звонки и заявки от новых клиентов</h3>
                        </div>
                        <div class="col-xl-3 col-md-6">
                                <img src="img/head.png" alt="">
                                <h3 class="about_h3">Регулярный поток целевых покупателей</h3>
                        </div>
                        <div class="col-xl-3 col-md-6">
                                <img src="img/sale.png" alt="">
                                <h3 class="about_h3">Быстрый старт онлайн-продаж</h3>
                        </div>

                </div>
                <div class="row">
                        <div class="col-xl-12">
                           <button class="button_blog" onclick="document.location='register.php'">Продавать товары</button>
                        </div>
                     </div>
        </div>
</section>
<section class="pricing">
        <div class="container">
                <div class="row justify-content-center">
                        <div class="col-xl-8">
                                <h2 class="about__title">Тарифы</h2>
                                <p class="about__text">Здесь вы можете выбрать подходящий тариф!</p>
                </div>
                </div>
                <div class="row justify-content-around">
                       <div class="col-xl-4 col-md-6">
                        <div class="price">
                                
                                <div class="price__head">
                                       <p>Стартовый<br/></p> 
                                        <sup>₽</sup> <span id="free" >3 000</span><sub> в месяц</sub>
                                </div>
                                <div class="wrapper">
                                        <p>Срок подключения</p>

                                        <input type="radio" name="slider" id="tab-1">
                                        <input type="radio" name="slider" id="tab-2" >
                                        <input type="radio" name="slider" id="tab-3">
                                    
                                        <header>
                                            <label for="tab-1" class="tab-1">1 мес.</label>
                                            <label for="tab-2" class="tab-2">3 мес.</label>
                                            <label for="tab-3" class="tab-3">1 год.</label>
                                            <div class="slider"></div>
                                    
                                        </header>
                                        
                                </div>
                                <div class="price__body">
                                        <ul>
                                                <li>10 исходящих сообщений в день</li>
                                                <li>100 лимит товаров</li>
                                                <li>Показ вашего телефона незарегистрированным посетителям</li>
                                                <li>Показ вашего E-mail и ссылки на ваш сайт/ соцсети</li>
                                                <li>Просмотр сайта без рекламы</li>
                                                <li>Статус верифицированной компании (при оплате по счету)</li>

                                        </ul>
                                </div>
                                <div class="price__footer">
                                        <button class="button"><a  class="price_a" href="/tarif-check.php?type=free">Подключить тариф</a></button>
                                </div>
                        </div>
                       </div>
                       <div class="col-xl-4 col-md-6">
                        <div class="price">
                                <div class="price__head">
                                       <p>Премиум<br/></p> 
                                        <sup>₽</sup><span id="premium"> 5 500</span><sub> в месяц</sub>
                                </div>
                                <div class="wrapper">
                                        <p>Срок подключения</p>

                                        <input type="radio" name="slider" id="tab11-1">
                                        <input type="radio" name="slider" id="tab11-2" checked>
                                        <input type="radio" name="slider" id="tab11-3">
                                    
                                        <header>
                                            <label for="tab11-1" class="tab11-1">1 мес.</label>
                                            <label for="tab11-2" class="tab11-2">3 мес.</label>
                                            <label for="tab11-3" class="tab11-3">1 год.</label>
                                            <div class="slider"></div>
                                    
                                        </header>
                                        
                                </div>
                                <div class="price__body">
                                        <ul>
                                                <li>1 200 открытий закупок (на срок 12 мес.)</li>
                                                <li>100 исходящих сообщений в день</li>
                                                <li>10 000 лимит товаров</li>
                                                <li>Показ вашего телефона незарегистрированным посетителям </li>
                                                <li>Просмотр контактов поставщиков</li>
                                                <li>Отключение сторонней рекламы на вашей странице и просмотр сайта без рекламы</li>
                                                <li>Ежедневная синхронизация товаров YML
                                                </li>
                                                <li>Статус верифицированной компании (при оплате по счету)</li>

                                        </ul>
                                </div>
                                <div class="price__footer">
                                        <button class="button" ><a class="price_a" href="/tarif-check.php?type=premium">Подключить тариф</a></button>
                                </div>
                        </div>
                       </div>
                       <div class="col-xl-4 col-md-6">
                        <div class="price">
                                <div class="price__head">
                                       <p>Бизнес<br/></p> 
                                        <sup>₽</sup><span id="bussnes">9 000</span><sub> в месяц</sub>
                                </div>
                                <div class="wrapper">
                                        <p>Срок подключения</p>

                                        <input type="radio" name="slider" id="tab1-1">
                                        <input type="radio" name="slider" id="tab1-2">
                                        <input type="radio" name="slider" id="tab1-3">
                                    
                                        <header>
                                            <label for="tab1-1" class="tab1-1">1 мес.</label>
                                            <label for="tab1-2" class="tab1-2">3 мес.</label>
                                            <label for="tab1-3" class="tab1-3">1 год.</label>
                                            <div class="slider"></div>
                                    
                                        </header>
                                        
                                </div>
                                <div class="price__body">
                                        <ul>
                                                <li>1 200 открытий закупок (на срок 12 мес.)</li>
                                                <li>30 исходящих сообщений в день</li>
                                                <li>1 000 лимит товаров</li>
                                                <li>Показ вашего телефона незарегистрированным посетителям </li>
                                                <li>Просмотр контактов поставщиков</li>
                                                <li>Отключение сторонней рекламы на вашей странице и просмотр сайта без рекламы</li>
                                                <li>Ежедневная синхронизация товаров YML
                                                </li>
                                                <li>Статус верифицированной компании (при оплате по счету)</li>

                                        </ul>
                                </div>
                                <div class="price__footer">
                                        <button id="bussness" class="button"><a class="price_a" href="/tarif-check.php?type=bussness">Подключить тариф</a></button>
                                </div>
                        </div>
                       </div>
                </div>
        </div>
</section>
<section class="team">
        <div class="container">
           <div class="row justify-content-center">
              <div class="col-xl-8">
                 <h2 class="about__title">С нами работают</h2>
                 <p class="about__text">В данном разделе представлены партнеры компании «Первый Поставщик» в Перми</p>
              </div>
           </div>
           <div class="row multiple-items justify-content-center">
              <div class="col-xl-4">
                 <img src="img/team1.jpg" alt="" height="200px">
                 <h4>Felix</h4>
                 <p>Партнер</p>
                 <div class="icons">
                    <a href="">
                       <span class="fa-stack fa-2x">
                          <i class="fas fa-square fa-stack-2x"></i>
                          <i class="fab fa-vk fa-stack-1x fa-inverse"></i>
                       </span>
                    </a>
                    <a href="">
                       <span class="fa-stack fa-2x">
                          <i class="fas fa-square fa-stack-2x"></i>
                          <i class="fab fa-facebook fa-stack-1x fa-inverse"></i>
                       </span>
                    </a>
                    <a href="">
                       <span class="fa-stack fa-2x">
                          <i class="fas fa-square fa-stack-2x"></i>
                          <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
                       </span>
                    </a>
                    <a href="">
                       <span class="fa-stack fa-2x">
                          <i class="fas fa-square fa-stack-2x"></i>
                          <i class="fab fa-instagram fa-stack-1x fa-inverse"></i>
                       </span>
                    </a>
                 </div>
              </div>
              <div class="col-xl-4">
                 <img src="img/team2.jpg" alt="" height="200px">
                 <h4>Cat Chow</h4>
                 <p>Партнер</p>
                 <div class="icons">
                    <a href="">
                       <span class="fa-stack fa-2x">
                          <i class="fas fa-square fa-stack-2x"></i>
                          <i class="fab fa-vk fa-stack-1x fa-inverse"></i>
                       </span>
                    </a>
                    <a href="">
                       <span class="fa-stack fa-2x">
                          <i class="fas fa-square fa-stack-2x"></i>
                          <i class="fab fa-facebook fa-stack-1x fa-inverse"></i>
                       </span>
                    </a>
                    <a href="">
                       <span class="fa-stack fa-2x">
                          <i class="fas fa-square fa-stack-2x"></i>
                          <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
                       </span>
                    </a>
                    <a href="">
                       <span class="fa-stack fa-2x">
                          <i class="fas fa-square fa-stack-2x"></i>
                          <i class="fab fa-instagram fa-stack-1x fa-inverse"></i>
                       </span>
                    </a>
                 </div>
              </div>
              <div class="col-xl-4">
                 <img src="img/team3.jpg" alt="" height="200px">
                 <h4>Dog Chow</h4>
                 <p>Партнер</p>
                 <div class="icons">
                    <a href="">
                       <span class="fa-stack fa-2x">
                          <i class="fas fa-square fa-stack-2x"></i>
                          <i class="fab fa-vk fa-stack-1x fa-inverse"></i>
                       </span>
                    </a>
                    <a href="">
                       <span class="fa-stack fa-2x">
                          <i class="fas fa-square fa-stack-2x"></i>
                          <i class="fab fa-facebook fa-stack-1x fa-inverse"></i>
                       </span>
                    </a>
                    <a href="">
                       <span class="fa-stack fa-2x">
                          <i class="fas fa-square fa-stack-2x"></i>
                          <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
                       </span>
                    </a>
                    <a href="">
                       <span class="fa-stack fa-2x">
                          <i class="fas fa-square fa-stack-2x"></i>
                          <i class="fab fa-instagram fa-stack-1x fa-inverse"></i>
                       </span>
                    </a>
                 </div>
              </div>
              <div class="col-xl-4">
                 <img src="img/team4.jpg" alt="" height="200px">
                 <h4>Gourmet</h4>
                 <p>Партнер</p>
                 <div class="icons">
                    <a href="">
                       <span class="fa-stack fa-2x">
                          <i class="fas fa-square fa-stack-2x"></i>
                          <i class="fab fa-vk fa-stack-1x fa-inverse"></i>
                       </span>
                    </a>
                    <a href="">
                       <span class="fa-stack fa-2x">
                          <i class="fas fa-square fa-stack-2x"></i>
                          <i class="fab fa-facebook fa-stack-1x fa-inverse"></i>
                       </span>
                    </a>
                    <a href="">
                       <span class="fa-stack fa-2x">
                          <i class="fas fa-square fa-stack-2x"></i>
                          <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
                       </span>
                    </a>
                    <a href="">
                       <span class="fa-stack fa-2x">
                          <i class="fas fa-square fa-stack-2x"></i>
                          <i class="fab fa-instagram fa-stack-1x fa-inverse"></i>
                       </span>
                    </a>
                 </div>
              </div>
           </div>
        </div>
     </section>
     <section class="service" id="blog">
        <div class="container">
           <div class="row justify-content-center">
              <div class="col-xl-8">
                 <h2 class="about__title">Акции</h2>
                 <p class="about__text">Ознакомиться с содержанием страницы Акции!</p>
              </div>
           </div>
           <?
                  require 'configDB.php';
                  $query=$pdo->query("SELECT * FROM `sales` ORDER BY `id` ASC");

                  while ($row =$query->fetch(PDO::FETCH_OBJ)) {
                  echo '
                  <div class="row '.$row->filter.' justify-content-between">
                
              <div class="blog-item ">
                 <img src="'.$row->img.'" alt="" width="90%">
              </div>
              <div class="blog-item ">
                 <div class="blog">
                    <a href="/sale.php?id='.$row->id.'">'.$row->header.'</a>
                    <div>
                       <span><i class="far fa-calendar-alt"></i> '.$row->time.'</span>
                       <span><i class="far fa-file"></i> Development</span>
                    </div>
                    <p>'.$row->description.'</p>
                 </div>
              </div>
            
           </div>


                       ';}?>
           
           <div class="row">
              <div class="col-xl-12">
                 <button class="button_blog" onclick="document.location='sales.php'">Смотреть ещё</button>
              </div>
           </div>
        </div>
    
</section>
<?require 'blocks/footer.php';?>