<?require 'blocks/header.php';?>
<section class="service">
        <div class="container">
                <div class="row justify-content-center">
                        <div class="col-xl-12 col-12">
                               <div class="breadcrumbs">
                                        <a href="">Главная</a> / <a href="">Каталог</a> / <a href="">Светильник настольный "Белый лебедь"</a>                          
                               </div>
                               
                </div>
                </div>
                <div class="row ">
                        <div class="col-xl-12">
                                <h2 class="about__title left">Добавить свою компанию</h2>
                                <p class="about__text left">Разместите заказ бесплатно за 2 минуты и получите выгодные предложения!</p>
                </div>
                </div>

				
				
	

														<form class="card-form" action="/add-company.php" method="post">

																<label>Описание для компании</label>
																<input type="text" class="input-field" name="company_name"  required id="task" placeholder="Название компании" style="margin-bottom:15px;"/>
																<input type="text" class="input-field" name="company_slogan"  required id="price" placeholder="Слоган компании" style="margin-bottom:15px;"/>
																<div class="select-wrapper">
																<select name="company_type" style="margin-bottom:15px;">
																    <optgroup label="Тип компании">
																     <option value="Производитель">Производитель</option>
																     <option value="Поставщик">Поставщик</option>
																    </optgroup>
																   </select>
																</div>
																<h4>Способы доставки</h4><br/>
														<div class="row text-left">
															<div class="col-xl-2">
															<label class="checkbox-other">
																<input type="checkbox" name="delivery_methods[]" value="Самовывоз">
																<span>Самовывоз</span>
																</label>
															</div>
															<div class="col-xl-2">
															<label class="checkbox-other">
																<input type="checkbox" name="delivery_methods[]" value="Транспортной компанией">
																<span>Транспортной компанией</span>
																</label>
															</div>
															<div class="col-xl-2">
															<label class="checkbox-other">
																<input type="checkbox" name="delivery_methods[]" value="Железной дорогой">
																<span>Железной дорогой</span>
																</label>
															</div>
															<div class="col-xl-2">
															<label class="checkbox-other">
																<input type="checkbox" name="delivery_methods[]" value="Курьером">
																<span>Курьером</span>
																</label>
															</div>
															<div class="col-xl-2">
															<label class="checkbox-other">
																<input type="checkbox" name="delivery_methods[]" value="Автомобилем">
																<span>Автомобилем</span>
																</label>
															</div>
															<div class="col-xl-2">
															<label class="checkbox-other">
																<input type="checkbox" name="delivery_methods[]" value="Авиатранспортом">
																<span>Авиатранспортом</span>
																</label>
															</div>
														</div>
														<h4>Варианты оплаты</h4><br/>
														<div class="row text-left">
															<div class="col-xl-2">
															<label class="checkbox-other">
																<input type="checkbox" name="payment_options[]" value="Безналичная оплата">
																<span>Безналичная оплата</span>
																</label>
															</div>
															<div class="col-xl-2">
															<label class="checkbox-other">
																<input type="checkbox" name="payment_options[]" value="Наличными">
																<span>Наличными</span>
																</label>
															</div>
														</div>
														<h4>Услуги</h4><br/>
														<div class="row text-left">
															<div class="col-xl-2">
															<label class="checkbox-other">
																<input type="checkbox" name="services[]" value="Для собак">
																<span>Для собак</span>
																</label>
															</div>
															<div class="col-xl-2">
															<label class="checkbox-other">
																<input type="checkbox" name="services[]" value="Для кошек">
																<span>Для кошек</span>
																</label>
															</div>
															<div class="col-xl-2">
															<label class="checkbox-other">
																<input type="checkbox" name="services[]" value="Для птиц">
																<span>Для птиц</span>
																</label>
															</div>
															<div class="col-xl-2">
															<label class="checkbox-other">
																<input type="checkbox" name="services[]" value="Для рептили">
																<span>Для рептили</span>
																</label>
															</div>
															<div class="col-xl-2">
															<label class="checkbox-other">
																<input type="checkbox" name="services[]" value="Для рыб">
																<span>Для рыб</span>
																</label>
															</div>
															<div class="col-xl-2">
															<label class="checkbox-other">
																<input type="checkbox" name="services[]" value="Гигиена и чистота">
																<span>Гигиена и чистота</span>
																</label>
															</div>
															<div class="col-xl-2">
															<label class="checkbox-other">
																<input type="checkbox" name="services[]" value="Корм">
																<span>Корм</span>
																</label>
															</div>
															<div class="col-xl-2">
															<label class="checkbox-other">
																<input type="checkbox" name="services[]" value="Для прогулок">
																<span>Для прогулок</span>
																</label>
															</div>
														</div>
														<h4>Объем поставок</h4><br/>
														<div class="row text-left">
															<div class="col-xl-2">
															<label class="checkbox-other">
																<input type="checkbox" name="scope_supply[]" value="Крупный опт">
																<span>Крупный опт</span>
																</label>
															</div>
															<div class="col-xl-2">
															<label class="checkbox-other">
																<input type="checkbox" name="scope_supply[]" value="Мелкий опт">
																<span>Мелкий опт</span>
																</label>
															</div>
														</div>	
	<br>

																<label>Контактные данные для компании</label>
																<input type="tel" class="input-field" name="tel_company"  required id="tel" placeholder="Телефон компании" style="margin-bottom:15px;"/>
																<input type="text" class="input-field" name="address_company"  required id="district" placeholder="Адрес компании" style="margin-bottom:15px;"/>
																<input type="text" class="input-field" name="email_company"  required id="district" placeholder="E-mail" style="margin-bottom:15px;"/>
																<input type="text" class="input-field" name="time_work"  required id="district" placeholder="Время работы" style="margin-bottom:15px;"/>
																<input type="text" class="input-field" name="site_company"  required id="district" placeholder="Сайт" style="margin-bottom:15px;"/>
																<label>Реквизиты компании</label>
																<input type="text" class="input-field" name="legal_name"  required id="district" placeholder="Юридическое название компании" style="margin-bottom:15px;"/>
																<input type="text" class="input-field" name="employees"  required id="district" placeholder="Кол-во сотрудников" style="margin-bottom:15px;"/>
																<input type="text" class="input-field" name="year_foundation"  required id="district" placeholder="Год основания" style="margin-bottom:15px;"/>
																<input type="text" class="input-field" name="OGRN"  required id="district" placeholder="ОГРН" style="margin-bottom:15px;"/>
																<input type="text" class="input-field" name="INN"  required id="district" placeholder="ИНН" style="margin-bottom:15px;"/>

																<textarea name="description_company" class="input-field" required id="des" placeholder="Описание компании" style="margin-bottom:15px;" value="Описание" onKeyDown="textCounter(this)" onKeyUp="textCounter(this)"></textarea>
															<div id="counter" style="float:right;"> 0/250</div>	<div style="float:right;margin-right:5px;">Кол-во символов :</div>

																<script type="text/javascript">function textCounter(el){
document.getElementById("counter").innerHTML =el.value.length+"/250";


}</script>

															<?php if($_COOKIE['update']=='Да'):
																?>
															<div class="action" style="float:left;margin-right:15px;">
																<input type="submit" name="button" class="action-button mb-3" value="Изменить услугу">
															</div>
														<?php else:?>
															<div class="action" style="display:flex;">
																<input type="submit" name="button" class="action-button mb-3" value="Добавить">
															</div>
														<?php endif;?>
														</form>
														<?php if($_COOKIE['update']=='Да'):
															?>
															<div class="action" style="float:left;">
														<a href='/exit.php'><input type="submit" name="button" class="action-button mb-3" value="Выйти из режима редактирования"></a>
														</div>

														<?php endif;?>
														<div style="display:flex;flex-wrap:wrap;">
														<form style="" action="/createfile.php" enctype="multipart/form-data" method="post">	
														<label>
														<input type="file" name="image" class="anons" />
														<span>Загрузить логотип для компании</span>
														</label>
														<button class="btn ml-3 mb-3 mr-3" name="image-brand"  style="background-color: #8ebebc;" ><i class="fa fa-download" aria-hidden="true" style="background-color: #8ebebc;color: white;" ></i></i></button>

															</form>
															<form style="" action="/createfile.php" enctype="multipart/form-data" method="post">	
														<label>
														<input type="file" name="image" class="anons" />
														<span>Загрузить фон для компании</span>
														</label>
														<button class="btn ml-3 mb-3" name="image-phon"  style="background-color: #8ebebc;" ><i class="fa fa-download" aria-hidden="true" style="background-color: #8ebebc;color: white;" ></i></i></button>

															</form>
														</div>
														<form name="uploader" enctype="multipart/form-data" method="POST" class="upload" id="upload">
																<span id="upload-text">

																Перетащите изображение в блок загрузки: </span><input name="userfile[]" type="file" multiple id="multiFiles" /><!-- multiple, который разрешает браузеру выбрать несколько файлов-->
																<div style="width:100%;margin-top:15px;" class="divId">

																</div>
																<input type="submit" name="submit" value="Загрузить" style="margin:20px; width: auto !important;"/>
														</form>
														

														<script type="text/javascript">

														$("form[name='uploader']").submit(function(e) {
															//  var formData = new FormData($(this)[0]);
															var form_data = new FormData();
															 var colorArray2 = document.body.getElementsByClassName("divId");
															 var innerHtml = "";
														var filesLength=document.getElementById('multiFiles').files.length;
														for(var i=0;i<filesLength;i++){
															 form_data.append("userfile[]", document.getElementById('multiFiles').files[i]);
															 var input = document.getElementById('multiFiles');
														//	 var elem=input.files[i].name;
															// innerHtml += '<div class="element">'+elem+'</div>';
															// alert(input.files[i].name);
																//colorArray2[i].innerHTML = innerHtml;
															}

																$.ajax({
																		url: 'file.php',
																		type: "POST",
																		data: form_data,
																		async: false,
																		success: function (msg) {
																				alert(msg);

																				for(var i=0;i<filesLength;i++){

																					 var elem=input.files[i].name;
																					 innerHtml += '<div class="element">'+elem+'</div>';
																					 if(filesLength<4){
																						 document.getElementById('upload').style.margin="15px 0px 230px 0px";
																					 }else if(filesLength<6){
																						document.getElementById('upload').style.margin="15px 0px 300px 0px";
																					}else if(filesLength<9){
																					 document.getElementById('upload').style.margin="15px 0px 370px 0px";
																				 }else if(filesLength<11){
																					 document.getElementById('upload').style.margin="15px 0px 470px 0px";

																				 }
																				//	alert(input.files[i].name);
																					//	colorArray2[i].innerHTML = innerHtml;
																					}
																					for(var i=0;i<filesLength;i++){
																							colorArray2[i].innerHTML = innerHtml;
																						}
																		},
																		error: function(msg) {
																				alert('Ошибка!');
																		},
																		cache: false,
																		contentType: false,
																		processData: false
																});
																e.preventDefault();
														});
														</script>


	<script type="text/javascript">

	jQuery('document').ready(function($) {
	 $('.upload').upload({
	 action:'obr.php',
	 label:'Перетащите файл в блок загрузки',
	 postKey:'newfile',
	 maxQueue:1,
	 maxSize:10485760,
	 postData:{
	 name:'User',
	 ip:'127.0.0.1'
	 }
	 })
	 });
</script>


</div>

</section>
<section class="about">
        <div class="container">
                <div class="row justify-content-center">
                        <div class="col-xl-8">
                                <h2 class="about__title">Ваша компания здесь!</h2>
                                <p class="about__text">Расскажите, что вы ищете!
                                        Мы отправим вашу заявку компаниям с похожими предложениями.
                                        С вами свяжется представитель компании продавца и расскажет о наличии и ценах.</p>
                </div>

                </div>
                <div class="row">
                        <div class="col-xl-12 mb-12">
						<?							  require 'configDB.php';
													  $query=$pdo->query("SELECT * FROM `company` ORDER BY `id_company` DESC");
	 												 $id=$_COOKIE['id'];
													 
													  while ($row =$query->fetch(PDO::FETCH_OBJ)) {
														if($_COOKIE['id']==$row->id_company){
															echo'
														 <button class="button_service" ><a class="company" href="/profi.php?company_id='.$row->id_company.'">Перейти к своей компании</a></button>';
														}
													 	
													  }?>
                        </div>
                </div>
        </div>
</section>



			

													<?require 'blocks/footer.php';?>