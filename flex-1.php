<!DOCTYPE html>
<html>
<head>
<style>

.container{
width:100%;
margin:0 auto;
display: flex;
flex-wrap: wrap;
max-height: 960px;

}
.block-first{
  background: black;
  min-height: 400px;
  font-size: 40px;
  color: white;
  flex:auto;
  display: flex;
  justify-content: center;
  align-items: center;
}
.container-right{
  margin-left: 30px;
  display: flex;
  flex: 0 0 220px;
  flex-direction: column;
}
.block-item{
    background: black;
    min-height: 200px;
    font-size: 40px;
    color: white;
    flex: auto;
    display: flex;
    justify-content: center;
    align-items: center;
margin-bottom: 10px;
}
.block-item:last-child{
margin-bottom: 0;
}
.top{
  background: black;
  width:100%;
  max-width: 960px;
  min-width: 460px;
  min-height:100px;
  margin: 0 auto 40px;
}
.grid{
  width:100%;
  max-width: 960px;
  margin:0 auto;
  padding: 0 30px;
  display: flex;
  min-width: 460px;
flex-wrap: wrap;
box-sizing: border-box;
}
.grid-item{
  background: black;
  min-height: 200px;
  font-size: 40px;
  color: white;
  margin:0 10px 20px;
  display: flex;
  flex:30px;
  justify-content: center;
  align-items: center;
}
</style>
</head>
<body>
  <div class="container">
<div class="block-first">
  1
</div>
<div class="container-right">
<div class="block-item">2</div>
<div class="block-item">3</div>

</div>
  </div>
  <div class="top"></div>
    <dib class="grid">
<div class="grid-item"></div>
<div class="grid-item"></div>
<div class="grid-item"></div>
<div class="grid-item"></div>

</div>


</body>
</html>
