<!DOCTYPE html>
<html>
<head>
<style>
html{
  background: white;
  min-height: 100%;
  display: flex;
  flex-direction: column;
}
body{
  margin: 0;
  padding: 0 15px;
  display: flex;
  flex-direction: column;
  flex:auto;
}
a{
  color: white;
  text-decoration: underline;
}
a:hover{
  text-decoration: underline;

}
img{
  border:0;
}
h1{
  margin-top: 0;
}
.header{
  width:100%;
  max-width: 960px;
  min-width:430px;
  margin: 0 auto 30px;
  padding: 30px 0 10px;
  display: flex;
  flex-wrap:wrap;
  align-items: flex-end;
  justify-content: space-between;
  box-sizing: border-box;

}
.logo{
  background: #000;
  width: 200px;
  height: 80px;
  font-size: 0;
  margin: 0 10px 10px 0;
  display: flex;
  flex: none;
  align-items: center;
}
.nav{
  margin:-5px 0 10px -15px;
  display: flex;
  flex-wrap: wrap;
}
.nav-itm{
  background: #000;
  width:130px;
  height: 50px;
  align-items: center;
  justify-content: center;
  display: flex;
  margin: 5px 0 0 5px;
  text-align: center;
}
.main{
  width: 100%;
  min-width: 430px;
  max-width: 960px;
  margin: auto;
  box-sizing: border-box;
  flex: auto;
}
.fline{
  background: #000;
  width: 70%;
  min-height: 80px;
  margin: 0 0 30px 30px;

}
.sline{
  background: #000;
  width: 80%;
  min-height: 80px;
  margin: 0 0 30px 30px;
}
.grid{
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
}
.grid-itm{
  margin-bottom:30px;
  flex:0 1 calc(25%-30*3/4);
}
.grid-img{
  width: 100%;
  height: auto;
  display: block;
  margin-bottom:30px;

}

.sqr{
  width: 200px;
  height: 200px;
  background: #000;

}
.footer{
  width: 100%;
  max-width: 960px;
  min-width: 430px;
  color: white;
  margin:auto;
  padding: 15px;
  box-sizing: border-box;
  background: #000;

}
@media screen and (max-width:800px){
  .grid-itm{
    flex:50%;
  }
}
</style>
</head>
<body>
  <header class="header">
    <a href="#" class="logo">Logo</a>
    <nav class="nav">
      <a href="#" class="nav-itm">13234</a>
      <a href="#" class="nav-itm">678687</a>
      <a href="#" class="nav-itm">57858</a>
    </nav>
  </header>
<main class="main">
<div class="fline"></div>
<div class="sline"></div>
<div class="grid">
  <div class="grid-itm">
    <div class="grid-img sqr">
    </div>
    <h1>Title</h1>
    <p>gugiu</p>
  </div>
  <div class="grid-itm">
    <div class="grid-img sqr">
    </div>
    <h1>Title</h1>
    <p>gugiu</p>
  </div>
  <div class="grid-itm">
    <div class="grid-img sqr">
    </div>
    <h1>Title</h1>
    <p>gugiu</p>
  </div>
  <div class="grid-itm">
    <div class="grid-img sqr">
    </div>
    <h1>Title</h1>
    <p>gugiu</p>
  </div>
</div>

</main>
<footer class="footer">

</footer>
</body>
</html>
