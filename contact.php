<?require 'blocks/header.php';?>

<style>

.ico-wrap {
    color: #ECAB00;
    width: 50px;
    height: 50px;
    margin: 25px 20px 2px 2px;
    display: block;
    text-align: center;
    font-size: 25px;
   
}

.descript-wrapper {
  text-align: left;
  margin-top: 20px;
  margin-bottom: 20px;
}
.aio-icon-title {
    font-size: 18px;
    color: #00897b;
    line-height: 1;
    margin-bottom: 10px;
}

.ico-wrap img{
  
}
@media (max-width: 767px) {
    .descript-wrapper,
    .ico-wrap {
      float: none;
      text-align: center;
    }
    .ico-wrap {
      margin: 0 auto 15px auto;
    }
}


  </style>
<section class="service">
        <div class="container">
                <div class="row justify-content-center">
                        <div class="col-xl-12 col-12">
                               <div class="breadcrumbs">
                                        <a href="">Главная</a> / <a href="">Контакт</a>                        
                               </div>
                               
                </div>

                </div>
                <div class="row ">
                        <div class="col-xl-12">
                                <h2 class="about__title left">Контакт</h2>
                </div>
                </div>
                
        </div>


        <div class="container">
  <div class="row ustify-content-center">

 <div class="col-xl-3 col-md-6">
    <div class="ico-wrap"> <img src="img/location.svg" />
    </div>
    <div class="descript-wrapper">
        <h4 class="aio-icon-title">Наш фактический адрес:</h4>
        <div class="aio-icon-description">
            192019, г. Санкт-Петербург, ул. Мельничная,
            <br>дом 22, литер А, офис 28</div>
    </div>
    </div>
    <div class="col-xl-3 col-md-6">

    <div class="ico-wrap">      <img src="img/clock.svg" width="35px" />
    </div>
    <div class="descript-wrapper">
        <h4 class="aio-icon-title">Время работы офиса и склада:</h4>
        <div class="aio-icon-description">
            пн.-пт. с 9:00 до 18:00, без перерыва.
            <br>сб, вс — выходной</div>
    </div>
    </div>
    <div class="col-xl-3 col-md-6">
    <div class="ico-wrap">
    <img src="img/mail_email.svg" width="35px" />
    </div>
    <div class="descript-wrapper">
        <h4 class="aio-icon-title">Электронная почта:</h4>
        <div class="aio-icon-description">
            transtrek.spb@mail.ru
            <br>transtrek.spb@mail.ru</div>
    </div>
    </div>
    
    <div class="col-xl-3 col-md-6">

    <div class="ico-wrap">
    <img src="img/phone.svg" width="35px" />
    </div>
    <div class="descript-wrapper">
        <h4 class="aio-icon-title">Наши Телефоны:</h4>
        <div class="aio-icon-description">
            8 (812) 703-70-46
            <br>8 (812) 703-70-46</div>
    </div>
    </div>


</div>
<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A486156ab5f2935733e2f0e834cb3f0c6c09c818266dcf9aa13d2ed269c93b49b&amp;width=100%&amp;height=400&amp;lang=ru_RU&amp;scroll=true"></script>
<br/>
<br/>
<br/>
</div>

     <!-- Contact -->
  <section id="contact" class="four">
        <div class="container">
    
                <div class="row justify-content-center">
                        <div class="col-xl-12">
                                <h2 class="about__title">Отправить предложение на заказ</h2>
                                <p class="about__text">Заполните форму: укажите цену за заказ и срок поставки или задайте вопрос. После этого ваше предложение получит заказчик. Если заказчик выберет вас, мы отправим вам его контактные данные. Нажимая кнопку «Отправить предложение», вы соглашаетесь с условиями использования и обработкой персональных данных. Ваше предложение увидит только заказчик.</p>
                </div>
    
          
                <div class="col-xl-12">
          <form method="post" action="#">
            <div class="row">
                <div class="col-xl-6">
                        <textarea name="message" placeholder="Message"></textarea>
                </div>
              <div class="col-xl-3 col-md-6">
                <input type="text" name="name" placeholder="Цена" />
                <input type="text" name="name" placeholder="Фамилия и имя" />
                <input type="text" name="name" placeholder="E-mail" />

        </div>
              <div class="col-xl-3 col-md-6">
                <input type="text" name="email" placeholder="Поставка, дни" />
                <input type="text" name="name" placeholder="Название компании" />
                <input type="text" name="name" placeholder="Телефон" />
                <input type="submit" value="Отправить предложение" />

        </div>
   
            </div>
          </form>
          </div>
    
        </div>
      </section> 
      <section class="about"  id="company">
        <div class="container">
                <div class="row justify-content-center">
                        <div class="col-xl-8">
                                <h2 class="about__title">Похожие заказы</h2>
                                <p class="about__text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed d</p>
                </div>
                </div>
                <?
                  require 'configDB.php';
                  $query=$pdo->query("SELECT * FROM `description` ORDER BY `id` ASC");

                  while ($row =$query->fetch(PDO::FETCH_OBJ)) {
                  echo '
                  <div class="col-xl-12" id="order'.$row->id.'" style="display:none;">
                  <a  href="/cart.php?cart_id='.$row->id.'" >
                     <div class="order">
                             <img src="uploads/1.jpg"/>
                             <div class="order-text">
                                     <h5>'.$row->description.'</h5>
                                     <span class="data" >'.$row->timeads.'</span>
                                     <br/>
                                     <span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star"></span>
<span class="fa fa-star"></span>
                                     <p>
             '.$row->des.'
                                     </p>
             <span class="card-meta"><div class="tag"><i class="fa fa-tag"></i>'.$row->price.'₽</div></span><br><br>
                                     <button class="button">Подробнее</button>

                             </div>
                             
                             
                     </div>
     </a></div>';

                      }
                     ?>
                <div class="row">
                        <div class="col-xl-12">
                           <button class="button_blog mb-5" id="5">Смотреть ещё</button>
                        </div>
                     </div>
                    
        </div>
</section>
<script type="text/javascript">

let k=0;
  while (k < 5) { // выводит 0, затем 1, затем 2
    $('#order'+k).show(500);
  k++;
  }
</script>
        <script type="text/javascript">
      


$( ".button_blog " ).click(function() {
  let i =0;
  var clickId = $(this).prop('id');
  while (i < clickId) { // выводит 0, затем 1, затем 2
    $('#order'+i).show(500);
  i++;
}
let age = Number(clickId);
age=age+5;

$(this).attr("id", age);
});


</script>
<?require 'blocks/footer.php';?>
<? //<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>?>
