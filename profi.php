<?require 'blocks/header.php';?>
<section class="service">
        <div class="container">
                <div class="row justify-content-center">
                        <div class="col-xl-12 col-12">
                               <div class="breadcrumbs">
                                        <a href="">Главная</a> / <a href="">Каталог</a> / <a href="">Светильник настольный "Белый лебедь"</a>                          
                               </div>
                               
                </div>

                </div>
                
        </div>


        <div class="container">
        <?
$company_id=$_GET['company_id'];
require 'configDB.php';
$query=$pdo->query("SELECT * FROM `company` ORDER BY `id_company` DESC");

while ($row =$query->fetch(PDO::FETCH_OBJ)) {

//echo  $tel;
if($company_id==$row->id_company){
?>
        <div class="col-xl-12 col-12">
        <?php
              echo'  <div style="background-image:url('.'uploads/phone-'.$row->id_company.'.jpg'.')" class="company-img">';?>
                
                <div class="header-box">
                        <div class="col-xl-4">
                               <?php
                                echo '<img src="uploads/brand-'.$row->id_company.'.jpg" alt="" class="brand">';?>
                        </div>
                        <div class="col-xl-8"> 
                                 <h1><?=$row->company_name?></h1>
                                 <p><?=$row->company_slogan?></p>
                </div>
               
                </div>
             
                
              
        </div>
        <div class="">
        <div class="tab">
  <button class="tablinks active" onclick="openCity(event, 'London')">О компании</button>
  <button class="tablinks" onclick="openCity(event, 'Paris')">Фото</button>
  <button class="tablinks" onclick="openCity(event, 'Tokyo')">Контакты</button>
</div>

<!-- Tab content -->
<div id="London" class="tabcontent" style="display: block;">
  <h3>О компании</h3>
  <p><?=$row->description_company?></p>
<div class="row">
        <div class="col-xl-6">
        <ul>
        <li><strong>Тип компании:</strong> <?=$row->company_type?></li>

        <?php if (isset($row->large_wholesale) || isset($row->small_wholesale) ): ?>
        <li><strong>Объем поставок:</strong> <ul class="list">
        <?php if (isset($row->large_wholesale)): ?>
                <li class="list"><?=$row->large_wholesale?></li>
        <?php endif; ?>
        <?php if (isset($row->small_wholesale)): ?>
                <li class="list"><?=$row->small_wholesale?></li>
        <?php endif; ?>
        </ul></li>
        <?php endif; ?>
        <?php if (isset($row->dog) || isset($row->cat) || isset($row->birds) || isset($row->reptiles) || isset($row->fish) || isset($row->cleanliness) || isset($row->feed) || isset($row->stroll)): ?>
        <li><strong>Услуги:</strong> <ul class="list">
                <?php if (isset($row->dog)): ?>
                <li class="list"><?=$row->dog?></li>
                <?php endif; ?>
                <?php if (isset($row->cat)): ?>
                <li class="list"><?=$row->cat?></li>
                <?php endif; ?>
                <?php if (isset($row->birds)): ?>
                <li class="list"><?=$row->birds?></li>
                <?php endif; ?>
                <?php if (isset($row->reptiles)): ?>
                <li class="list"><?=$row->reptiles?></li>
                <?php endif; ?>
                <?php if (isset($row->fish)): ?>
                <li class="list"><?=$row->fish?></li>
                <?php endif; ?>
                <?php if (isset($row->cleanliness)): ?>
                <li class="list"><?=$row->cleanliness?></li>
                <?php endif; ?>
                <?php if (isset($row->feed)): ?>
                <li class="list"><?=$row->feed?></li>
                <?php endif; ?>
                <?php if (isset($row->stroll)): ?>
                <li class="list"><?=$row->stroll?></li>
                <?php endif; ?>
        </ul></li>
        <?php endif; ?>
        <li><strong>Юр. лицо:</strong> <ul class="list">
                <li class="list"><?=$row->legal_name?></li>
                <li class="list">Кол-во сотрудников: <?=$row->employees?> чел.</li>
                <li class="list">Год основания: <?=$row->year_foundation?></li>
                <li class="list">ОГРН: <?=$row->OGRN?></li>
                <li class="list">ИНН: <?=$row->INN?></li>
        </ul></li>
</ul>
</div>
<div class="col-xl-6">
<ul>
<?php if (isset($row->cashless_payment) || isset($row->cash) ): ?>
        <li><strong>Варианты оплаты:</strong> 
        <ul class="list">
        <?php if (isset($row->cashless_payment)): ?>
                <li class="list"><?=$row->cashless_payment?></li>
                <?php endif; ?>
                <?php if (isset($row->cash)): ?>
                <li class="list"><?=$row->cash?></li>
                <?php endif; ?>

        </ul></li>
        <?php endif; ?>
        <?php if (isset($row->pickup) || isset($row->transport) || isset($row->by_rail) || isset($row->by_courier) || isset($row->by_car)): ?>
        <li><strong>Способы доставки:</strong> <ul class="list">
        <?php if (isset($row->pickup)): ?>
                <li class="list"><?=$row->pickup?></li>
                <?php endif; ?>
                <?php if (isset($row->transport)): ?>
                <li class="list"><?=$row->transport?></li>
                <?php endif; ?>
                <?php if (isset($row->by_rail)): ?>
                <li class="list"><?=$row->by_rail?></li>
                <?php endif; ?>
                <?php if (isset($row->by_courier)): ?>
                <li class="list"><?=$row->by_courier?></li>
                <?php endif; ?>
                <?php if (isset($row->by_car)): ?>
                <li class="list"><?=$row->by_car?></li>
                <?php endif; ?>
                <?php if (isset($row->by_air)): ?>
                <li class="list"><?=$row->by_air?></li>
                <?php endif; ?>

        </ul>
        </li>
        <?php endif; ?>
</ul>
</div>
</div>

</div>

<div id="Paris" class="tabcontent">
<div class="row">
<?php 
$i = 0;
$image="";
while($i <= 5){
        $image="uploads/z-$row->id_company$i.jpg";
       // echo  $image;
        if (file_exists($image)){
        ?>
  <div class="col-xl-4 col-md-6 col-12 mb-3">
      
  <a href="uploads/z-<?=$row->id_company?><?=$i?>.jpg" data-fancybox="images" data-caption="Backpackers following a dirt trail">
    <img src="uploads/z-<?=$row->id_company?><?=$i?>.jpg" alt="Snow" style="width:100%">
    </a>
  </div>
  <?
     $i++;
}else{ $i++; }

}
  ?>
</div>
</div>

<div id="Tokyo" class="tabcontent">
  <h3>Контакты</h3>
  

<div class="row">
        <div class="col-xl-6">
        <ul>
        <li class="mb-3"><strong><img class="icon-company" class="mt-3" src="img/place.svg" width="30px" height="30px" />Адрес:</strong><?=$row->address_company?>.</li>
        <li class="mb-3"><strong><img class="icon-company" class="mt-3" src="img/phone.svg" width="30px" height="30px" />Телефоны:</strong> <a href="tel:<?=$row->tel_company?>" class="phone_number"><?=$row->tel_company?></a><span class="phone_number_active show">Показать </span></li>
        <li class="mb-3"><strong><img class="icon-company" class="mt-3" src="img/internet.svg" width="30px" height="30px" />Сайт:</strong>  <a href="<?=$row->site_company?>" ><?=$row->site_company?></a></li>
</ul>
        </div>
        <div class="col-xl-6">
        <ul>
        <li class="mb-3"><strong><img class="icon-company" class="mt-3" src="img/mail_email.svg" width="28px" height="28px" />E-mail:</strong>  <a href="<?=$row->email_company?>" ><?=$row->email_company?></a></li>
        <li class="mb-3"><strong><img class="icon-company" class="mt-3" src="img/clock.svg" width="28px" height="28px" />Время работы:</strong> <?=$row->time_work?></li>
        <li class="mb-3"><strong><img class="icon-company" class="mt-3" src="img/mail_email.svg" width="28px" height="28px" />Отправить сообщение</strong> <a href="#contact" >- Нажать</a></li>

</ul>
        </div>

</div>
</div>
        </div>
                             
        <?
}}
?>
                  
    
        </div>
        
      </section> 
      <section id="contact" class="four">
        <div class="container">
    
                <div class="row justify-content-center">
                        <div class="col-xl-12">
                                <h2 class="about__title">Отправить сообщение</h2>
                                <p class="about__text">Заполните форму: укажите цену за заказ и срок поставки или задайте вопрос. После этого ваше предложение получит заказчик. Если заказчик выберет вас, мы отправим вам его контактные данные. Нажимая кнопку «Отправить предложение», вы соглашаетесь с условиями использования и обработкой персональных данных. Ваше предложение увидит только заказчик.</p>
                </div>
    
          
                <div class="col-xl-12">
          <form method="post" action="#">
            <div class="row">
                <div class="col-xl-6">
                        <textarea name="message" placeholder="Message"></textarea>
                </div>
              <div class="col-xl-6 ">

                <input type="text" name="name" placeholder="Фамилия и имя" />
                <input type="text" name="name" placeholder="Телефон" />
                <input type="text" name="name" placeholder="E-mail" />
                <input type="submit" value="Отправить предложение" />


        </div>   
            </div>
          </form>
          </div>
    
        </div>
      </section> 
      <section class="about"  id="company">
      <div class="container">
                <div class="row justify-content-center">
                        <div class="col-xl-8">
                                <h2 class="about__title">Похожие заказы</h2>
                                <p class="about__text">Рассмотрите заказы у других поставщиков</p>
                </div>
                </div>
                <?
                  require 'configDB.php';
                  $query=$pdo->query("SELECT * FROM `description` ORDER BY `id` ASC");
                  $k=1;
                 
                  
                  while ($row =$query->fetch(PDO::FETCH_OBJ)) {
                    $image="uploads/zav-$row->id.jpg";
                  echo '
                  <div class="col-xl-12" id="order'.$k.'" style="display:none;">
                  <a  href="/cart.php?cart_id='.$row->id.'" >
                     <div class="order">';
if (file_exists($image)){
                     echo'
                             <img src="uploads/zav-'.$row->id.'.jpg" width="250px" style="max-width:100%;"/>
                             <div class="order-text">
                                     <h5>'.$row->description.'</h5>
                                     <span class="data" >'.$row->timeads.'</span>
                                     <br/>
                                     <span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star"></span>
<span class="fa fa-star"></span>
                                     <p>
             '.$row->des.'
                                     </p>
             <span class="card-meta"><div class="tag"><i class="fa fa-tag"></i>'.$row->price.'₽</div></span><br><br>
                                     <button class="button">Подробнее</button>

                             </div>
                             
                             
                     </div>
     </a></div>';}else{
      echo'
      <img src="img/no_photo.png" width="250px" style="max-width:100%;"/>
      <div class="order-text">
              <h5>'.$row->description.'</h5>
              <span class="data" >'.$row->timeads.'</span>
              <br/>
              <span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star"></span>
<span class="fa fa-star"></span>
              <p>
'.$row->des.'
              </p>
<span class="card-meta"><div class="tag"><i class="fa fa-tag"></i>'.$row->price.'₽</div></span><br><br>
              <button class="button">Подробнее</button>

      </div>
      
      
</div>
</a></div>';
     }

     $k++;  }
                     ?>
                <div class="row">
                        <div class="col-xl-12">
                           <button class="button_blog mb-5" id="5">Смотреть ещё</button>
                        </div>
                     </div>
                    
        </div>
</section>
<script type="text/javascript">
        function openCity(evt, cityName) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}
</script>
<script type="text/javascript">
//document.getElementById('event').onclick = function (event) {
     //   document.getElementById("navbarSupportedContent").classList.add('show');
    //}
   // $( document ).ready(function(){
	//  $( "#event" ).click(function(){ // задаем функцию при нажатиии на элемент с классом slide-toggle
	//    $( "#navbarSupportedContent" ).slideToggle(400); // плавно скрываем, или отображаем все элементы <div>
	//  });
	//});



        $(function(){
        let holder=$('.phone_number'),
        button=$('.phone_number_active'),
        number=holder.text(),
        symbolsForHide=9,
        show=()=>{
          holder.text(number)
          button.removeClass('show').text('Скрыть')
        },
        hide=()=>{
          holder.text(number.replace(new RegExp('(.+).{'+symbolsForHide+'}$'),"$1"+'x'.repeat(symbolsForHide)))
          button.addClass('show').text('Показать')
        }
        button.click(function(){
          if($(this).hasClass('show')) show()
          else hide()
        })
        hide()
        })
</script>

<script type="text/javascript">
		$(document).ready(function() {
			/*
			*   Examples - images
			*/

			$("a#example1").fancybox();

			$("a#example2").fancybox({
				'overlayShow'	: false,
				'transitionIn'	: 'elastic',
				'transitionOut'	: 'elastic'
			});

			$("a#example3").fancybox({
				'transitionIn'	: 'none',
				'transitionOut'	: 'none'	
			});

			$("a#example4").fancybox({
				'opacity'		: true,
				'overlayShow'	: false,
				'transitionIn'	: 'elastic',
				'transitionOut'	: 'none'
			});

			$("a#example5").fancybox();

			$("a#example6").fancybox({
				'titlePosition'		: 'outside',
				'overlayColor'		: '#000',
				'overlayOpacity'	: 0.9
			});

			$("a#example7").fancybox({
				'titlePosition'	: 'inside'
			});

			$("a#example8").fancybox({
				'titlePosition'	: 'over'
			});

			$("a[rel=example_group]").fancybox({
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'titlePosition' 	: 'over',
				'titleFormat'		: function(title, currentArray, currentIndex, currentOpts) {
					return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
				}
			});

		});

                
	</script>
<script type="text/javascript">

let k=0;
  while (k < 5) { // выводит 0, затем 1, затем 2
    $('#order'+k).show(500);
  k++;
  }
</script>
        <script type="text/javascript">
      


$( ".button_blog " ).click(function() {
  let i =0;
  var clickId = $(this).prop('id');
  while (i < clickId) { // выводит 0, затем 1, затем 2
    $('#order'+i).show(500);
  i++;
}
let age = Number(clickId);
age=age+5;

$(this).attr("id", age);
});


</script>
<?require 'blocks/footer.php';?>
