<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <title>Hello World</title>
    <script src="https://unpkg.com/react@17/umd/react.development.js"></script>
    <script src="https://unpkg.com/react-dom@17/umd/react-dom.development.js"></script>

    <!-- Don't use this in production: -->
    <script src="https://unpkg.com/@babel/standalone/babel.min.js"></script>
</head>
<body>
<div id="example"></div>
<div id="program"></div>

<script type="text/babel">
    class Block extends React.Component {
        render() {
            const num = 20;
            return (
                <div>
                    <h2>It is {num}</h2>
                    <p>Это простой текст{7+9}</p>
                </div>
            );
        }
    }
    class Game extends React.Component {
        render() {
            return (
                <div>
                    <h2>{this.props.name}</h2>
                    <p>{this.props.age}</p>
                </div>
            );
        }
    }
    const prom=document.getElementById("program");
    ReactDOM.render(
        <div>
            <Game name="Forza" age="18"/>
            <Game name="Sims" age="12"/>

        </div>

        ,prom);

    ReactDOM.render(<div><Block /><Block /><Block /></div>,document.getElementById("example"));

//    ReactDOM.render(<div><h1>354 HFD</h1></div>,document.getElementById("example"));

</script>
<!--
  Note: this page is a great way to try React but it's not suitable for production.
  It slowly compiles JSX with Babel in the browser and uses a large development build of React.

  Read this section for a production-ready setup with JSX:
  https://reactjs.org/docs/add-react-to-a-website.html#add-jsx-to-a-project

  In a larger project, you can use an integrated toolchain that includes JSX instead:
  https://reactjs.org/docs/create-a-new-react-app.html

  You can also use React without JSX, in which case you can remove Babel:
  https://reactjs.org/docs/react-without-jsx.html
-->
</body>
</html>