<!DOCTYPE html>
<html lang="en">
<head>
      <meta charset="UTF-8">
    <title>Title</title>
    <meta name="viewport" content="width-device-width, initial-scale=1.0" >
    <meta http-equiv="X-UA-Conpatible" content="ie-edge">
    <link rel="stylesheet" href="css/h2.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
          integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
   
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />    
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.4.1/dist/jquery.min.js"></script>
  <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>

 
</head>
<body>
<div id="toTop"><i class="fas fa-chevron-up"></i></div>
<section class="header-top-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-xl-5">
                        <div class="header-top-content">
                            <ul class="nav nav-pills navbar-left">
                                <li><a href="#"><i class="pe-7s-call"></i><span>123-123456789</span></a></li>
                                <li><a href="#"><i class="pe-7s-mail"></i><span> info@mart.com</span></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6 col-xl-7">
                        <div class="header-top-menu">
                            <ul class="nav nav-pills navbar-right">
                                <li><a href="company.php">Компания</a></li>
                                <li><a href="news.php">Новости</a></li>
                           
                                <li><a href="sales.php">Акции</a></li>
                                <?php if($_COOKIE["id"]==""):?>
                                <li><a href="/register.php"><i class="pe-7s-lock"></i>Login/Register</a></li>
                                <?php else: ?>
                                <li><a href="/exit.php"></i>Выйти</a></li>
                                <?php endif;?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <header class="main_menu home_menu">
<div class="container">
<div class="row align-items-center justify-content-center">
<div class="col-lg-12">
<nav class="navbar navbar-expand-lg navbar-light">
<a class="navbar-brand" href="/"> <img src="data:image/webp;base64,UklGRlwGAABXRUJQVlA4TFAGAAAvu0AJEN/mNpIkSckefnACk/Bf/kV0lnYDQiTJsqIsbhE4wAT+ReDj2m3HkSQ5UTbaBzAG/63hBycbkm27bRtDhPAd0L2M0vsse8gSsv9BnzWaKTwBQwWBEQjkeXZX+AKv44icJhkMq6w2MH7wo1y3YAjB8JqieF0gbMsSEYAOA7QAUB0OgAEoCEocCAFMCYYmPZSQYUplQINJD0ChICiAAIQBChAHQ0AQhPKPBkNDRTMOph0YCoQBjkIIhkIdnzBpcFQsK8H8ocDQUOGo8osEQ0NFg6Hgh1YUcVNoDWoYsTTEEpCIiKNgayLiICAImIgIEl5pxQ+KiQVRBhu0Ya3uAeYy0oFg2C+cjKsrS09u/qCgvEwxPmfBTCwYZsmHteQweGXpRxUJ6Yc6/e79/2uBYz82/+V5jz63bff9fQdgTrekj22vlx1fm38uC9/vR7099qnPfOJ/3G3635tcEnKql5xxZMWRSzIZLjkk5K6LzFxOy1xuMZ/1bs2cNAEREaaj7X8aMfqHurTMcACsmRTscoJ0WTwDV8zlVgwH4N2/5xvTgDxrDVh7HI9lh1RH9H8CZNu2bdrRTMq2bdv2HbFt55Rt27Ztmyd1/+3sfbNv8B7R/wmQ8GRZlimU5fp/lbjWKiV46iNKq4bepY+lpAHYrOpERNQvJrTXceQaoPASqnYMlBpBDtjU0ldnQDqKHgdkKgM6INfISRqYybDJGNZxow1kMnQN2KfX5sWLls2WhY0OJgtotNex97Z+/tG3/25Jg2tSi2m3qp9K/W7dGtIYBh0f+iuCC8eH94xOzMxbd3oZtIFaRIdRkQxsnz0m8NJHrQewRmoNdJHWQHUjaAVMsh0CDkbHBz4ZLBQikusQzop4aHQ4Y45Jqi4o2NcUhL6ZwneBAw0lBS8i5NrPYUWkADfA2R9//PHY17eN+eabrI2PaabNNKAENtIi1z6OLQ4mA7wv7TeNMUOj0eldj98NijeBlimR9ezWJiYaC1htI9cwxzaFZrB9d4yZKJXCeQcxexcDcWXbJbXLoiLQZhnrAn2zmOVkfuAqEbSfkgKsWtM6cJPMra9ql7JmA9bFKqDuINcQR4dk4Ifa+8iY8dI9OFq/2LlYR0m/E+CxpEGQF5Y0G446yIC+0rMVZFg+LcOc/0/SPZiXDXg3bPGBpItcuxwBk6dw3Biz0dVVYPb+3beAD9JUGCapCvgkhROhh4ORsDCskzDSUg5zu369nAsjTcbTr705wG3PG6kUJIBctzlCLLh+T3799dfDF28ac+eJo5gcKJUUToISaQtUSm2zgFPSK1giB6+rYW/7DFZ8N9WN807EShoLhZbZr3xfkg98kiRCHJBrywHZKsT2C3xXjr4AmyTpBqxqoWdZhFrrPCzjvnQNRkr7Ex4YEy5YnkDKBDjqm6w910O8KdGX0Y04IFeRGsjUPgsPP9jpag/QLvAR+CbdhAGaTuIgeKKF8EoajHWwRR5A6q8Ifm+7XpRD0DRO9SqCxAFTSRrIVIJ8v8evvvrqmbcO/CZtJ/2Bv4EdQG/pCkz4tYqxHeM40x2WhaX9uQ+MufttX0LAZdm25gNkrohgjRMfIA6YNpCpBBSDvCLBTvYA7QIfgW9SB0i8AB9UyqLLMFn1NWksFPi2zyEoutr5195ozGARIo52phLSwOjZ+wZsCoyGjDpJhXCPFXW6DPdgm6uO3vwust0ET5LOOIjNgt2BGiZB4oBMJSSBTJ692Fy4JSmcCsWStB5gstQmE8jo5MpsyYeNkv6kOtASmPlbkgJVVVVd4shUOsqqqkpYPQcaDngHLs4DdgWeBHZKmg/UqEFMgvQj3fbcxcVYIKn4ZkeZ0L1NapWuzuR5CN/COkbBmGTI8SWdBQY2jHd5GBe4+LGC4HPx/boDCun593VjToXdhPPS/0QePpKGwURJir10H4gr7yvzpVDcUEnqWE7a3/rVLSfrpS02iVAfSd2rHkL+4N7ZLJV0E4bZ9HPiomwyOogUsLHtXt5aa2eA7/MMt3rb47cat/+2p6LtgVoGz6GQqJ1Bkwy1hnESN1JA4wfRNeAlcnUDLAcY5cBConeUAaXvoVUDLCSCdQZQ/qddybwBxl7i2I5pl/OqqqppSrtIJJbVjulZeIlpdZNxV7ZKJL69X1hb+5HEMQ==" alt="logo" data-pagespeed-url-hash="2890529547" onload="pagespeed.CriticalImages.checkImageForCriticality(this);"> </a>
<button class="navbar-toggler collapsed" id="event" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
<span class="menu_icon"><i class="fas fa-bars"></i></span>
</button>
<div class="navbar-collapse main-menu-item collapse" id="navbarSupportedContent" style="">
<ul class="navbar-nav">
<li class="nav-item">
<a class="nav-link" href="catalog.php">Заказы</a>
</li>
<li class="nav-item">
<a class="nav-link" href="companies.php">Поставщики</a>
</li>
<li class="nav-item">
<a class="nav-link" href="tarif.php">Тарифы</a>
</li>
<li class="nav-item dropdown">
<a class="nav-link dropdown-toggle" href="" id="navbarDropdown_3" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
Каталог
</a>
<div class="dropdown-menu" aria-labelledby="navbarDropdown_2">
<a class="dropdown-item" href="/catalog.php?category=dog">
Для собак
</a>
<a class="dropdown-item" href="/catalog.php?category=cat">Для кошек
</a>
<a class="dropdown-item" href="/catalog.php?category=birds">Для птиц</a>
<a class="dropdown-item" href="/catalog.php?category=reptiles">Для рептилии
</a>
<a class="dropdown-item" href="/catalog.php?category=fish">Для рыб
</a>
<a class="dropdown-item" href="/catalog.php?category=cleanliness">Гигиена и чистота
</a>
<a class="dropdown-item" href="/catalog.php?category=feed">Корм
</a>
<a class="dropdown-item" href="/catalog.php?category=stroll">Для прогулок
</a>
</div>
</li>

<li class="nav-item">
<a class="nav-link" href="contact.php">Контакты</a>
</li>
<li class="nav-item dropdown">
<a class="nav-link dropdown-toggle" href="" id="navbarDropdown_3" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
<i class="fa fa-search" aria-hidden="true"></i>
</a>
<div class="dropdown-menu search" aria-labelledby="navbarDropdown_2">
<form method="post" action="search.php">
<input  class="dropdown-item" type="search" name="search_text" id="search_text" class="nav-link" 
     placeholder="Search..."
     aria-label="Search through site content">

     </form>

</div>
</li>
</ul>
</div>

</nav>
</div>
</div>
</div>
<div class="search_input" id="search_input_box" style="display: none;">
<div class="container ">
<form class="d-flex justify-content-between search-inner">
<input type="text" class="form-control" id="search_input" placeholder="Search Here">
<button type="submit" class="btn"></button>
<span class="ti-close" id="close_search" title="Close Search"></span>
</form>
</div>
</div>
</header>


        <style>
                .header-top-section {
    display: block;
    width: 100%;
    background: #00897b;
    color: #fff;
}
.header-top-content ul li a {
    color: #fff;
    font-size: 14px;
    font-weight: 500;
    padding-left: 0;
}
.nav > li > a {
    position: relative;
    display: block;
    padding: 10px 15px;
}
.header-top-menu ul li a {
    color: #fff;
    font-size: 14px;
    font-weight: 500;
}
.nav-pills > li + li {
    margin-left: 2px;
}
.nav {
    padding-left: 0;
    margin-bottom: 0;
    list-style: none;
}
@media (min-width: 768px){
.navbar-right {
    float: right !important;
    margin-right: -15px;
}}
@media (min-width: 768px){
.navbar-left {
    float: left !important;
}}
@media (max-width: 767px){
.header-top-menu, .header-top-content {
    text-align: center;
}}
@media (max-width: 767px){
.header-top-menu ul, .header-top-content ul {
    display: inline-block;
}}
.nav-pills > li {
    float: left;
}

/**************menu part start*****************/
/* line 3, /Applications/MAMP/htdocs/old/palash/cl/shop_html/sass/_menu.scss */
.main_menu .navbar-brand {
  padding-top: 0rem;
  padding-bottom: 0px;
}

/* line 7, /Applications/MAMP/htdocs/old/palash/cl/shop_html/sass/_menu.scss */
.main_menu .navbar {
  padding: 0px;
}

/* line 12, /Applications/MAMP/htdocs/old/palash/cl/shop_html/sass/_menu.scss */
.main_menu .main-menu-item ul li .nav-link {
  color: #4B3049;
  font-size: 16px;
  padding: 35px 21px;
  font-family: "Rubik", sans-serif;
  text-transform: capitalize;
  line-height: 16px;
  font-weight: 400;
}

/* line 20, /Applications/MAMP/htdocs/old/palash/cl/shop_html/sass/_menu.scss */
.main_menu .main-menu-item ul li .nav-link:hover {
  color: #795376 !important;
}

.collapse{
  transition: all 0.5s;
}

@media only screen and (min-width: 992px) and (max-width: 1200px) {
  /* line 12, /Applications/MAMP/htdocs/old/palash/cl/shop_html/sass/_menu.scss */
  .main_menu .main-menu-item ul li .nav-link {
    padding: 35px 20px;
  }
}


/* line 35, /Applications/MAMP/htdocs/old/palash/cl/shop_html/sass/_menu.scss */
.main_menu #search_input_box {
  position: fixed;
  left: 50%;
  -webkit-transform: translateX(-50%);
  -moz-transform: translateX(-50%);
  -ms-transform: translateX(-50%);
  -o-transform: translateX(-50%);
  transform: translateX(-50%);
  width: 100%;
  max-width: 1140px;
  z-index: 999;
  text-align: center;
  background: #B08EAD;
  box-shadow: 0 10px 15px rgba(0, 0, 0, 0.2);
}

/* line 49, /Applications/MAMP/htdocs/old/palash/cl/shop_html/sass/_menu.scss */
.main_menu #search_input_box ::placeholder {
  color: #fff;
}

@media (max-width: 991px) {
  /* line 35, /Applications/MAMP/htdocs/old/palash/cl/shop_html/sass/_menu.scss */
  .main_menu #search_input_box {
    margin-top: 15px;
  }
}

/* line 58, /Applications/MAMP/htdocs/old/palash/cl/shop_html/sass/_menu.scss */
.main_menu #search_input_box .form-control {
  background: transparent;
  border: 0;
  color: #ffffff;
  font-weight: 400;
  font-size: 15px;
  padding: 0;
}

/* line 66, /Applications/MAMP/htdocs/old/palash/cl/shop_html/sass/_menu.scss */
.main_menu #search_input_box .btn {
  width: 0;
  height: 0;
  padding: 0;
  border: 0;
}

/* line 72, /Applications/MAMP/htdocs/old/palash/cl/shop_html/sass/_menu.scss */
.main_menu #search_input_box .ti-close {
  color: #fff;
  font-weight: 600;
  cursor: pointer;
  padding: 10px;
  padding-right: 0;
}

/* line 79, /Applications/MAMP/htdocs/old/palash/cl/shop_html/sass/_menu.scss */
.main_menu .search-inner {
  padding: 5px 5px;
}

/* line 82, /Applications/MAMP/htdocs/old/palash/cl/shop_html/sass/_menu.scss */
.main_menu .form-control:focus {
  box-shadow: none;
}

/* line 85, /Applications/MAMP/htdocs/old/palash/cl/shop_html/sass/_menu.scss */
.main_menu .active_color {
  color: #f5790b !important;
}

/* line 89, /Applications/MAMP/htdocs/old/palash/cl/shop_html/sass/_menu.scss */
.main_menu a i {
  color: #4B3049 !important;
  position: relative;
  z-index: 1;
  font-size: 20px;
}

input[type="search"]{
    background-color: white !important;
    border-radius:5px;
    outline: 0;
    color:black;
}
.dropdown .search{
  background-color:transparent !important
}

/* line 98, /Applications/MAMP/htdocs/old/palash/cl/shop_html/sass/_menu.scss */
.main_menu .cart .dropdown-toggle::after {
  display: inline-block;
  margin-left: .255em;
  vertical-align: .255em;
  content: "";
  border: 0px solid transparent;
}

/* line 106, /Applications/MAMP/htdocs/old/palash/cl/shop_html/sass/_menu.scss */
.main_menu .cart i {
  position: relative;
  z-index: 1;
}

/* line 113, /Applications/MAMP/htdocs/old/palash/cl/shop_html/sass/_menu.scss */
.menu_fixed {
  position: fixed;
  z-index: 9999 !important;
  width: 100%;
  box-shadow: 0px 10px 15px rgba(0, 0, 0, 0.05);
  top: 0;
  background-color: #fff;
}

/* line 121, /Applications/MAMP/htdocs/old/palash/cl/shop_html/sass/_menu.scss */
.menu_fixed .active_color {
  color: #B08EAD !important;
}

/* line 127, /Applications/MAMP/htdocs/old/palash/cl/shop_html/sass/_menu.scss */
.dropdown .dropdown-menu {
  transition: all 0.5s;
  overflow: hidden;
  transform-origin: top center;
  transform: scale(1, 0);
  display: block;
  border: 0px solid transparent;
  background-color: #00897b;
}

/* line 135, /Applications/MAMP/htdocs/old/palash/cl/shop_html/sass/_menu.scss */
.dropdown .dropdown-menu .dropdown-item {
  font-size: 14px;
  padding: 8px 20px !important;
  background-color: #00897b;
  text-transform: capitalize;
}
.dropdown .dropdown-menu a.dropdown-item{
  color: white !important;
}


  /* line 147, /Applications/MAMP/htdocs/old/palash/cl/shop_html/sass/_menu.scss */
  .dropdown:hover .dropdown-menu {
    transform: scale(1);
    box-shadow: 0 10px 40px rgb(0 0 0 / 10%);
  }
  /* line 154, /Applications/MAMP/htdocs/old/palash/cl/shop_html/sass/_menu.scss */
  .dropdown:hover .dropdown-menu {
    display: block;
  }



@media (max-width: 576px) {
  /* line 241, /Applications/MAMP/htdocs/old/palash/cl/shop_html/sass/_menu.scss */
  .main_menu {
    padding: 20px 0px;
  }
}

@media (max-width: 1000px){
  /* line 247, /Applications/MAMP/htdocs/old/palash/cl/shop_html/sass/_menu.scss */
  .main_menu {
    padding: 20px 0px;
  }
  .main_menu .main-menu-item ul li .nav-link{
    padding: 20px 0px;
  }
  .dropdown .dropdown-menu{
    display: none !important;
  }
  .dropdown:hover .dropdown-menu {
    display: block !important;
  }
  .dropdown{
    transition: all 0.5s !important;
  }

  
}

/* line 252, /Applications/MAMP/htdocs/old/palash/cl/shop_html/sass/_menu.scss */
.dropdown-menu {
  border: 0px solid rgba(0, 0, 0, 0.15) !important;
  background-color: #fafafa;
}

/* line 259, /Applications/MAMP/htdocs/old/palash/cl/shop_html/sass/_menu.scss */
.main-menu-item {
  justify-content: center !important;
}

                </style>
