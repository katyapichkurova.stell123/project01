<?require 'blocks/header.php';?>
<section class="service">
        <div class="container">
                <div class="row justify-content-center">
                        <div class="col-xl-12 col-12">
                               <div class="breadcrumbs">
                                        <a href="">Главная</a> / <a href="">Компания</a>                           
                               </div>
                               
                </div>
                </div>
                <div class="row">
                        <div class="col-xl-3">
                                <div    class="menu_top_block">
                                        <ul>
                                                <li class="bordered active"><a href="/company.php">Компания</a></li>
                                                <li class="bordered"><a href="/news.php">Новости</a></li>
                                                <li class="bordered"><a href="/sales.php">Акции</a></li>
                                                <li class="bordered"><a href="/rekvizity.php">Карточка компании</a></li>
                                        </ul>
                                </div>
                        </div>
                        <div class="col-xl-9">
                                <p class="about__text left mb-5">Первый Поставщик — это бесплатная база поставщиков и производителей по поиску поставщиков. Помогает налаживать контакты с потенциальными партнерами и заказчиками по России и находить новых клиентов.</p>
                                        
                              
                                <div class="row justify-content-around mb-5">
                        <div class="col-xl-3 col-md-6">
                                <img src="img/internet.png" alt="">
                                <h3 class="about_h3">Дополнительная точка для продаж в интернете</h3>
                        </div>
                        <div class="col-xl-3 col-md-6">
                                <img src="img/transfer.png" alt="">
                                <h3 class="about_h3">Звонки и заявки от новых клиентов</h3>
                        </div>
                        <div class="col-xl-3 col-md-6">
                                <img src="img/head.png" alt="">
                                <h3 class="about_h3">Регулярный поток целевых покупателей</h3>
                        </div>
                        <div class="col-xl-3 col-md-6">
                                <img src="img/sale.png" alt="">
                                <h3 class="about_h3">Быстрый старт онлайн-продаж</h3>
                        </div>

                </div>
                                <div class="row ">
                                                <h2 class="about__title left" >Как работает Первый Поставщик?</h2>   
                                <div class="row justify-content-center">
                                        <div class="col-xl-4 col-md-6">
                                                <p class="count">01</p>
                                                <h6 class="about__title left"><strong>Заказчик размещает заявку на любую продукцию</strong></h6>
                                                <p class="about__text left">Всего 1 минута требуется на размещение. Можно публиковать неограниченное число заказов.</p>
                                        </div>
                                        <div class="col-xl-4 col-md-6">
                                                <p class="count">02</p>
                                                <h6 class="about__title left"><strong>Поставщики предлагают цены</strong></h6>
                                                <p class="about__text left">Мы оповещаем поставщика о новом заказе по почте, после чего он предлагает свои цены на продукцию.</p>
                                        </div>
                                        <div class="col-xl-4 col-md-6">
                                                <p class="count">03</p>
                                                <h6 class="about__title left"><strong>Заказчик выбирает поставщика</strong></h6>
                                                <p class="about__text left">Из всех полученных предложений заказчик может выбрать любое подходящее ему и связаться с поставщиком.</p>
                                        </div>
                                </div>
                                <h2 class="about__title left" >Как работает Первый Поставщик?</h2>
                                <div class="row">
                                <div class="col-xl-12 col-md-12">
                                        <h6 class="about__title left"><strong>Найти поставщика — бесплатно!</strong></h6>
                                        <div class="text-button">
                                        <p class="about__text left">Для покупателей платформа бесплатна. Можно размещать неограниченное число заказов, выбирать товары в каталоге и просматривать базу поставщиков.</p>
                                        <button class="button" onclick="document.location='companies.php'">Найти поставщика</button>
                                        </div>
                                </div>
                                <div class="col-xl-12 col-md-12">
                                        <h6 class="about__title left"><strong>Найти поставщика — бесплатно!</strong></h6>
                                        <div class="text-button">
                                        <p class="about__text left">Для покупателей платформа бесплатна. Можно размещать неограниченное число заказов, выбирать товары в каталоге и просматривать базу поставщиков.</p>
                                        <button class="button" onclick="document.location='tarif.php'">Выбырать тариф</button>
                                        </div>
                                </div>
                                </div>

                        </div>
                </div>

           </div>
</section>
<?require 'blocks/footer.php';?>