<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <title>Hello World</title>
    <script src="https://unpkg.com/react@17/umd/react.development.js"></script>
    <script src="https://unpkg.com/react-dom@17/umd/react-dom.development.js"></script>
    <!-- Don't use this in production: -->
    <script src="https://unpkg.com/@babel/standalone/babel.min.js"></script>
    <style>
        body {
            background-color: #e1e388;
        }

        .field {
            display: flex;
            padding: 10px;
            width: 25%;
            margin-left: 37.5%;
            color: #fff;
            font-size: 1.3em;
            flex-direction: column;
            align-items: center;
        }

        .box {
            border: 2px solid #aaa719;
            background-color: #d4d37f;
            margin-bottom: 20px;
            padding: 10px;
            width: 80%;
        }

        .box:last-child {
            margin-bottom: 0;
        }

        button.btn {
            margin-right: 10px;
            padding: 10px;
            margin-top: 10px;
            cursor: pointer;
            font-size: 0.65em;
            color: #fff;
            outline: none;
        }

        .btn.light {
            background-color: #4eafc9;
            border: 2px solid #2a89a2;
        }

        .btn.red {
            background-color: #dda66d;
            border: 2px solid #b6821a;
        }

        .btn.light:hover {
            background-color: #2a89a2;
            border-color: #1d6f85;
        }

        .btn.red:hover {
            background-color: #b6821a;
            border-color: #946912;
        }
        .btn.danger {
            background-color: #dd6da1;
            border: 2px solid #b6821a;
            margin-bottom: 10px;
        }
    </style>


</head>
<body>

<div id="program"></div>

<div id="example"></div>
<script type="text/babel">



    class Task extends React.Component {
        constructor(props) {
            super(props);
            this.state = {
                edit: false,
            };
        };
        edit= () =>{
            this.setState({edit:true});
        };

        remove= () => {
            this.props.delete(this.props.index);
        };
        save= () =>{
            var value=this.refs.newTxt.value;
            this.props.update(this.refs.newTxt.value,this.props.index);
            this.setState({edit:false});
        };
        rendEdit= () =>{
            const num = 20;
            return (
                <div className="box">
                    <textarea ref="newTxt" defaultValue={this.props.children}></textarea>
                    <button onClick={this.save} className="btn light" >Сохранить </button>
                </div>
            );
        };
        rendNorm=() => {
            const num = 20;
            return (
                <div className="box">
                    <div className="text">{this.props.children}</div>
                    <button onClick={this.edit} className="btn light">Редактировать </button>
                    <button onClick={this.remove} className="btn red">Удалить </button>
                </div>
            );
        };
        render() {
           if(this.state.edit){
               return this.rendEdit();
           }else{
               return this.rendNorm();

           }

        }
    }

    class Field extends React.Component {
        constructor(props) {
            super(props);
            this.state = {
                tasks: []
            };
        };
        add=(text)=>{
          var arr = this.state.tasks;
          arr.push(text);
          this.setState ({tasks: arr});

        }
        deleteBlock = (i) => {
            var arr = this.state.tasks;
            arr.splice (i, 1);
            this.setState ({tasks: arr});
        };

        updateText = (text, i) => {
            var arr = this.state.tasks;
            arr[i] = text;
            this.setState ({tasks: arr});
        };
        eachTask = (item, i) => {
            return (
                <Task key={i} index={i} update={this.updateText} delete={this.deleteBlock}>
                    {item}
                </Task>
            );
        };
        render() {
            return (
                <div className="field">
                <button className="btn danger" onClick={this.add.bind(null,'Простое задание')}>Новое задание</button>
                    {this.state.tasks.map (this.eachTask)}
                </div>
            );
        }

    }


    class Check extends React.Component {
        constructor(props) {
            super(props);
            this.state = {
                checked: true,
            };
        };

        handleCheck = () => {
            this.setState({checked: !this.state.checked});
        };

        render() {
            var message;
            if (this.state.checked) {
                message = 'выбран';
            } else {
                message = 'не выбран';
            }

            return (
                <div>
                    <input type="checkbox" onChange={this.handleCheck} defaultChecked={this.state.checked} />
                    <p>Чекбокс {message}</p>
                </div>
            );
        }
    }


    const prom=document.getElementById("program");
    const app=document.getElementById("example");

    ReactDOM.render(<Check/>,app);

    ReactDOM.render(
       <Field/>

    ,prom);

    //    ReactDOM.render(<div><h1>354 HFD</h1></div>,document.getElementById("example"));

</script>
<!--
  Note: this page is a great way to try React but it's not suitable for production.
  It slowly compiles JSX with Babel in the browser and uses a large development build of React.

  Read this section for a production-ready setup with JSX:
  https://reactjs.org/docs/add-react-to-a-website.html#add-jsx-to-a-project

  In a larger project, you can use an integrated toolchain that includes JSX instead:
  https://reactjs.org/docs/create-a-new-react-app.html

  You can also use React without JSX, in which case you can remove Babel:
  https://reactjs.org/docs/react-without-jsx.html
-->
</body>
</html>
