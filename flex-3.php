<!DOCTYPE html>
<html>
<head>
<style>
html{
  background: #fff;
  min-height: 100%;
  display: flex;flex-direction: column;
}
body{
  margin: 0;
  padding: 0 15px;
  display: flex;
  flex-direction: column;
  flex:auto;
}
a{
  color: #fff;
}
img{
  border: 0;
}
h1{
  margin-top:0;
}
.header{
  width:100%;
  max-width: 960px;
  min-width:430px;
  margin: 0 auto 30px;
  padding: 30px 0 10px;
  display: flex;
  flex-wrap:wrap;
  align-items: flex-end;
  justify-content: space-between;
  box-sizing: border-box;
}
.logo{
  background: #000;
  width: 150px;
  height: 50px;
  font-size: 0;
  margin: 0 10px 40px 0;
  display: flex;
  flex: none;
  align-items: center;
}
.header-right{
  background: #000;
  min-height: 50px;
  font-size: 20px;
  color: #fff;
  margin-bottom: 40px;
  flex-basis: 300px;
  display: flex;
  justify-content: center;
  align-items: center;
}
.header-bottom{
  background: #000;
min-height: 50px;
font-size: 20px;
color: white;
flex-basis: 100%;
display: flex;
justify-content: center;
align-items: center;

}
.main{
  width:100%;
  max-width:960px;
  min-width: 430px;
  margin: auto;
  flex:auto;box-sizing: border-box;
}
.grid{
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
}
.grid-itm{
  margin-bottom:30px;
  flex-basis: 30%;
}
.grid-img{
  width:200px;
  height:200px;
  background: #000;
  margin: 0 auto  20px;
  flex 0 1 80%;
}
.grid-title{
  text-align: center;
}
.grid-cont{
  flex: 0 1 100%;
}
.footer{
  width: 100%;
  max-width: 960px;
  min-width: 430px;
  color: white;
  margin:auto;
  padding: 15px;
  box-sizing: border-box;
  background: #000;

}
@media screen and (max-width:800px){
  .header-right{
    display: none;
  }
  .grid-itm{
    flex-wrap: nowrap;
    flex-basis: 100%;
  }
  .grid-img{
    flex: 0 0 auto;
  }
  .grid-itm:nth-child(event) .grid-img{
    margin: 0 0 0 30px;
    order:2;
  }
  .grid-itm:nth-child(odd) .grid-img{
    margin: 0 30px 0 0;
  }
  .grid-title{
    text-align: left;
  }
}

</style>
</head>
<body>
  <header class="header">
<a href="" class="logo">Logo</a>
<div class="header-right">Content</div>
<div class="header-bottom">2</div>
  </header>
<main class="main">
<div class="grid">
  <div class="grid-itm">
    <div class="grid-img">

    </div>
    <div class="grid-cont">
      <h1 class="grid-title">hguigiugui</h1>
      <p>
        jkbbbbbbbbbbbbbbbb kjbbbbbbbb jkkkkkkkkk jkbbbbbbbbbbbb
      </p>
    </div>
  </div>
  <div class="grid-itm">
    <div class="grid-img">

    </div>
    <div class="grid-cont">
      <h1 class="grid-title">hguigiugui</h1>
      <p>
        jkbbbbbbbbbbbbbbbb kjbbbbbbbb jkkkkkkkkk jkbbbbbbbbbbbb
      </p>
    </div>
  </div>
  <div class="grid-itm">
    <div class="grid-img">

    </div>
    <div class="grid-cont">
      <h1 class="grid-title">hguigiugui</h1>
      <p>
        jkbbbbbbbbbbbbbbbb kjbbbbbbbb jkkkkkkkkk jkbbbbbbbbbbbb
      </p>
    </div>
  </div>
</div>
</main>
<footer class="footer">

</footer>
</body>
</html>
