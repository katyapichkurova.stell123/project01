<?require 'blocks/header.php';?>
<section class="service">
        <div class="container">
                <div class="row justify-content-center">
                        <div class="col-xl-12 col-12">
                               <div class="breadcrumbs">
                                        <a href="">Главная</a> / <a href="">Каталог</a> / <a href="">Светильник настольный "Белый лебедь"</a>                          
                               </div>
                               
                </div>
                </div>
                <div class="row ">
                        <div class="col-xl-12">
                                <h2 class="about__title left">Личный кабинет</h2>
                                <p class="about__text left">Разместите заказ бесплатно за 2 минуты и получите выгодные предложения!</p>
                </div>
                </div>
                <div class="tab">
  <button class="tablinks active" onclick="openCity(event, 'London')">Регистрация</button>
  <button class="tablinks" onclick="openCity(event, 'Paris')">Авторизация</button>

</div>

<div id="London" class="tabcontent" style="display: block;">

<form method="post" action="check.php">
                          <div class="row justify-content-center">
                            <div class="col-xl-16 col-md-6">
                            <input type="text" required   placeholder="Имя" name="name">
                              <input type="email" required name="login" placeholder="E-mail">
                              <input type="password" required name="pass" placeholder="Пароль">
                              <input type="password" required name="pass-2" placeholder="Повтори пароль">
                              <input type="submit" value="Регистрация">
                              <p>Нажимая кнопку «Оформить заказ», 
                                вы соглашаетесь с условиями использования 
                                и обработкой персональных данных</p>
              
                      </div>
                           
                 
                          </div>
                        </form>
                        
            
</div>

                <div id="Paris" class="tabcontent" >

                <form method="post" action="auth.php">
                          <div class="row justify-content-center">
                            <div class="col-xl-16 col-md-6">
                              <input type="email" required name="login" placeholder="E-mail">
                              <input type="password" required name="pass" placeholder="Пароль">
                              <input type="submit" value="Войти">
                              <p>Нажимая кнопку «Оформить заказ», 
                                вы соглашаетесь с условиями использования 
                                и обработкой персональных данных</p>
              
                      </div>
                           
                 
                          </div>
                        </form>
</div>      
                
</section>
<script type="text/javascript">
        function openCity(evt, cityName) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}
</script>

<?require 'blocks/footer.php';?>