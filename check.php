<?php
$login=filter_var(trim($_POST['login']),FILTER_SANITIZE_STRING);
$name=filter_var(trim($_POST['name']),FILTER_SANITIZE_STRING);
$pass=filter_var(trim($_POST['pass']),FILTER_SANITIZE_STRING);
$pass_2=filter_var(trim($_POST['pass-2']),FILTER_SANITIZE_STRING);

if(mb_strlen($login)<5 || mb_strlen($login)>100){
    
    require "blocks/header.php";
    echo "<div class='container'>
<h1 class='text-center mt-2' style='font-family: BlinkMacSystemFont'>Ошибка</h1>
<p class='text-center mt-3'>
Длина логина должна составлять от 5 до 100 символов, попробуйте еще раз!
<a href='register.php'>Вернуться назад</a>
</p>
</div>";
    require "blocks/footer.php";
    exit();
}elseif (mb_strlen($name)<3 || mb_strlen($name)>50){
    require "blocks/header.php";
    echo "<div class='container'>
<h1 class='text-center mt-2' style='font-family: BlinkMacSystemFont'>Ошибка</h1>
<p class='text-center mt-3'>
Длина имени пользователя должна составлять от 3 до 50 символов!
<a href='register.php'>Вернуться назад</a>
</p>
</div>";
    require "blocks/footer.php";
    exit();
}elseif (mb_strlen($pass)<6 || mb_strlen($pass)>32){
    require "blocks/header.php";
    echo "<div class='container'>
<h1 class='text-center mt-2' style='font-family: BlinkMacSystemFont'>Ошибка</h1>
<p class='text-center mt-3'>
Длина пароля должна составлять от 6 до 32 символов
<a href='register.php'>Вернуться назад</a>
</p>
</div>";
    require "blocks/footer.php";
    exit();
}elseif ($pass!=$pass_2){
    require "blocks/header.php";
    echo "<div class='container'>
<h1 class='text-center mt-2' style='font-family: BlinkMacSystemFont'>Ошибка</h1>
<p class='text-center mt-3'>
Повторный пароль введен неправильно!
<a href='register.php'>Вернуться назад</a>
</p>
</div>";
    require "blocks/footer.php";
    exit();
}
$host ='localhost';
$user='root';
$password='root';
$data='ads';
$pass=md5($pass."fgjfjd");
$link = mysqli_connect($host , $user,$password,$data);
$result =  mysqli_query($link,"INSERT INTO `check` (`login`, `pass`, `name`) VALUES ('$login', '$pass', '$name')");

mysqli_close($link);
header('Location:check.php');
?>