<!DOCTYPE html>
<html>
<head>
<style>

img{
  max-width: 100%;
}
body{
font-size: 17px;
line-height: 1.4;
}
.container{
  max-width:1100px;
  margin: 0 auto;
}
.cards{
display: flex;
justify-content: center;
flex-wrap: wrap;
margin-top: 15px;
padding: 1.5%;
box-sizing: border-box;
}
.card{
  width: 50%;
  position: relative;
  margin-bottom:20px;
  padding-bottom: 30px;
  background-color: white;
  color: white;
  text-decoration: none;
  box-shadow: rgba(0,0,0,0.19) 0 0 8px 0;
  border-radius: 4px;
}
@media (max-width: 600px){
  .card{
    width:100%;
  }
}
@media (max-width: 700px){
  .card{
    max-width:320px;
    margin-right: 20px;
    margin-bottom: 20px;
  }
}
.card span{
  display: block;
}
.card .card-summary{
  padding: 5% 5% 3% 5%;
  color: black;
}
.card .card-header{
  position: relative;
  height: 175px;
  overflow: hidden;
  background-repeat: no-repeat;
  background-position: center;
  background-color: rgba(255,255,255,0.15);
  background-blend-mode: overlay;
  border-radius: 4px 4px 0 0;
}
.card .card-header:hover,.card .card-header:focus{
  background-color: rgba(255,255,255,0);
}
.card .card-title{
  background: rgba(104,156,210,0.85);
  padding: 3.5% 0 2.5% 0;
  color: white;
  text-transform: uppercase;
  position: absolute;
  width: 100%;
}
.card .card-title h3{
  font-size: 1.2em;
  line-height: 1.2;
  margin: 0;
  padding: 0 3.5%;
}
.card .card-meta{
  max-height: 0;
  overflow: hidden;
  color: black;
  text-transform: uppercase;
  position: absolute;
  bottom: 5%;
  padding: 0 5%;
  transition-property: max-height;
  transition-duration: 0.4s;
  transition-timing-function: ease-in-out;
}
.card:hover, .card:focus{
  background: white;
  box-shadow: rgba(0,0,0,0.25) 0px 0px 20px 0px;
}

</style>
</head>
<body>
  <div class="container">
<div class="cards">
  <a href="" class="card">
    <span class="card-header" style="background-image:url('https://drasler.ru/wp-content/uploads/2019/05/HD-картинки-на-рабочий-стол-1366х768-Природа-сборка-3.jpg');">
<span class="card-title">
  <h3>Title</h3>
</span>
</span>
<span class="card-summary">
  GIUGUIG IHOIHOII OIHI
</span>
    <span class="card-meta">Meta post:28900 77 7</span>
  </a>
</div>

</div>


</body>
</html>
